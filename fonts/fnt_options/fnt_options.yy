{
    "id": "0cd7f806-4c34-430c-bdee-72fab6b494d8",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_options",
    "AntiAlias": 0,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Perfect DOS VGA 437 Win",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "116ea98a-821c-46fc-ae19-c98701504103",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "64ea79f7-cf0c-45cc-baa2-3c2ab22e21a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 27,
                "offset": 3,
                "shift": 15,
                "w": 8,
                "x": 152,
                "y": 118
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "5adfdd9a-5303-4a55-b46d-a1b5d1996c74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 27,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 15,
                "y": 118
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "5c26463e-05be-47e4-9710-d831c2c9a91e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 128,
                "y": 60
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "e3a91ea8-fbc5-446a-9bea-46bd9a7cb417",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 142,
                "y": 60
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "240c403a-cf2d-43eb-8116-a6dade3555b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 156,
                "y": 60
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "2f7e149d-97e1-4f96-b14c-8d8c41171064",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 170,
                "y": 60
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "ba6d741f-aeec-4bae-b2d2-29057894bfe6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 27,
                "offset": 2,
                "shift": 15,
                "w": 6,
                "x": 238,
                "y": 118
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "33f59909-49ca-4328-971c-4fa814d6bf67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 27,
                "offset": 3,
                "shift": 15,
                "w": 8,
                "x": 182,
                "y": 118
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "5e4e940b-4950-43af-90db-01acbef995d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 27,
                "offset": 3,
                "shift": 15,
                "w": 8,
                "x": 192,
                "y": 118
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "1f9fcec7-fc01-4f6e-bd01-096d9e12f47c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "56c40c33-6d3c-4f89-bf96-0804a7ff03d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 27,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 54,
                "y": 118
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "61037ab2-c0a9-417e-9a8a-1bb1e2684329",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 27,
                "offset": 3,
                "shift": 15,
                "w": 6,
                "x": 222,
                "y": 118
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "0c299825-fb57-45f7-9d5c-dcd2e20ebd7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 184,
                "y": 60
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "f3a7d9ad-9df2-4ad6-baac-a2ab4e032b40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 27,
                "offset": 5,
                "shift": 15,
                "w": 4,
                "x": 2,
                "y": 147
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "17648ddc-0115-4c15-962f-aefd49a761e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 198,
                "y": 60
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "05f5a312-fe5d-4ad8-b05a-c1b19c992e7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 212,
                "y": 60
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "09580fe4-a03f-405a-a6ee-9ed70e1ffad7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 27,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 106,
                "y": 118
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "772b5b98-e827-432d-a45a-e3dbce978a3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 226,
                "y": 60
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "eae21c0a-a522-4aa8-890a-7ed5836d273a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 72,
                "y": 89
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "74b2e8ba-dc79-4498-977d-f11898b3ee64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 240,
                "y": 60
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "ca7db9ca-5178-4adb-8af9-8de852f3e115",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 2,
                "y": 89
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "dea3cfb6-d56f-4704-8359-32e338ed2adb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 16,
                "y": 89
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "f9f89ffa-c56e-4c70-b82b-2d2cf6fd7e8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 30,
                "y": 89
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "8ad05904-cd29-4397-85c3-beff1c5fc328",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 44,
                "y": 89
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "8910e856-4fb5-4532-a983-bb416a029274",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 58,
                "y": 89
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "83260fcc-a5c7-41ed-93df-639f3d674634",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 27,
                "offset": 5,
                "shift": 15,
                "w": 4,
                "x": 246,
                "y": 118
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "26ca25a0-6b62-4283-a9cf-3d4254a86ba0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 27,
                "offset": 3,
                "shift": 15,
                "w": 6,
                "x": 230,
                "y": 118
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "5db00178-5a61-4f57-b5fe-8cd4bf9a40d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 27,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 41,
                "y": 118
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "5a20ba60-e9f2-4be6-ac26-b78374f37228",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 27,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 119,
                "y": 118
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "0d797d60-90e8-458a-9a12-673f42a11afc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 27,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 80,
                "y": 118
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "2382468f-176d-4b66-a8a5-e158193dda44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 212,
                "y": 89
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "cb25b961-68ad-4f73-b838-b4022f39555d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 184,
                "y": 89
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "5542727b-25f0-4e17-8a92-b1cbf6f0f692",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 170,
                "y": 89
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "c20a4be0-70fc-4160-9b7b-924244a1ac9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 86,
                "y": 89
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "5c7537c9-8263-4b8a-900a-2e568a36bf07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 156,
                "y": 89
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "98e91ece-32d5-400c-8cdb-b8a2bef31adb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 142,
                "y": 89
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "db10169c-5af5-4414-90c3-20cbf4340dc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 128,
                "y": 89
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "8b9a1923-4d7e-4944-a4d1-2db1c013e35a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 114,
                "y": 89
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "e232afa5-b306-4b34-acd1-29f32e528e87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 100,
                "y": 89
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "70268401-357f-4d4d-8eda-a709d5e13b4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 114,
                "y": 60
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "d6385fd5-103e-4412-9a7d-9e701ee22b20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 27,
                "offset": 3,
                "shift": 15,
                "w": 8,
                "x": 142,
                "y": 118
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "982cf368-05a0-444e-bc13-0cfcce1d69e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 86,
                "y": 60
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "dd4938e9-6bda-4743-8ec4-b469f884e483",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 72,
                "y": 60
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "d9d7028e-f789-4444-a3ea-59877efa0f99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "86e0cb5a-1c5f-48f7-8c56-218667886534",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 30,
                "y": 31
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "5c24f4c8-4cbb-484d-97ea-7dc5c5fcb227",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "0336951b-c2a5-43c4-bd3d-0cf8f573b92e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 100,
                "y": 60
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "d76eec0c-c728-466f-b6a9-20268cb16cca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 2,
                "y": 31
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "d4d9a571-6200-4adc-97a2-21fb187ecf8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 233,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "1c8e6216-3403-4a7f-9987-4a978c312337",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 219,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "23bccee5-419a-4fd4-8136-79694ab8f9c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 205,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "5719868a-e5c3-49e1-a276-4d630eafb707",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 27,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 226,
                "y": 89
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "45d7d736-f077-4303-8d9d-c9d203735a4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 191,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "845bc200-7aa9-43c0-b75c-8984245eb7c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 163,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "eb9eb33a-4f97-4a15-80aa-19fedceb9060",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "95ec3010-4823-4627-8669-cc89b1c19370",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 135,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "db81ba54-c5c1-486c-9515-fbce9ef1599f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 27,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 28,
                "y": 118
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "b4a259a1-dd42-4118-920b-89fba9eef33b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 121,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "b3aa5ac3-f424-4dbb-8930-8b3e2445cfc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 27,
                "offset": 3,
                "shift": 15,
                "w": 8,
                "x": 212,
                "y": 118
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "422245dd-8f32-4056-85ae-18e18b3e1565",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "bb13afdf-cc2b-4530-8226-d459f394c7b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 27,
                "offset": 3,
                "shift": 15,
                "w": 8,
                "x": 162,
                "y": 118
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "246f1750-792d-4e45-8e89-f300a04c15f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 93,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "55a9151d-6b04-482a-9781-19b504159a5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 19,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "38eb2278-ae2c-40eb-a83c-d7f2830834ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 27,
                "offset": 3,
                "shift": 15,
                "w": 8,
                "x": 202,
                "y": 118
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "40aac72c-bf45-45c5-8346-e312b2a1b277",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "7144fe5e-9a1d-425e-bb8e-728ee715919a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "81cd852c-3425-41ad-b4e4-dc3d256f2ce8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 44,
                "y": 31
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "f589da6c-759c-40bd-b6aa-3c91cb03dd3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 58,
                "y": 31
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "38420357-d482-47c9-a8d5-c2bc6eff0507",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 72,
                "y": 31
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "d4c7daf6-57d5-4625-b674-1a44e4b443b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 27,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 67,
                "y": 118
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "1e4da8b0-6942-41e7-aefd-fc70fb9df471",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 58,
                "y": 60
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "f750fc0d-c444-4a84-a16a-d74a141586b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 44,
                "y": 60
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "2d2ef576-d285-4860-8693-4ad506985770",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 27,
                "offset": 3,
                "shift": 15,
                "w": 8,
                "x": 172,
                "y": 118
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "b7fadf9c-f978-4691-b714-c7351311eef7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 27,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 93,
                "y": 118
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "d674a38f-35f7-4b12-8587-7530ad72de28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 30,
                "y": 60
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "69aefa13-2a98-4174-923d-c613fa6b539e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 27,
                "offset": 3,
                "shift": 15,
                "w": 8,
                "x": 132,
                "y": 118
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "d32f28ed-a252-4e3e-88bf-1814af22e551",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 16,
                "y": 60
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "bbce44d1-ed79-4435-942b-2896084fe468",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 2,
                "y": 60
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "6f5aba9f-9912-4a73-b8a1-572afe57c4bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 240,
                "y": 31
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "037c6762-c88e-4e92-b55d-9dbaae9f5a93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 226,
                "y": 31
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "4e190bc6-0b89-43a4-a7b0-2538fc018f14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 212,
                "y": 31
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "e7ee0e9e-35b7-4c71-88fe-6f7ae08b3dd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 198,
                "y": 31
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "7563041b-9937-4d07-a78c-731b7ce75b36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 184,
                "y": 31
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "db8f8b28-6dbd-4359-b2c1-8963cc3d8444",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 170,
                "y": 31
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "9a9509f9-a8a8-4404-afd6-095da088fb36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 156,
                "y": 31
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "fcbcf860-52a9-4037-812b-ea7350c50fc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 142,
                "y": 31
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "b9303785-8434-4464-a373-eed9dc0d3867",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 128,
                "y": 31
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "01bf5950-0082-4cba-92b4-c4914b8bbc4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 114,
                "y": 31
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "6e456bd3-0670-4991-9169-c7f445d3fa73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 100,
                "y": 31
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "9ba46efe-9c83-4a84-a7b2-1d12b8e62347",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 86,
                "y": 31
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "0757d539-9aea-47ed-a0da-8304eadbbccd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 27,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 2,
                "y": 118
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "e27f9c3e-1e26-4e21-8bac-59cf4f75d3fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 27,
                "offset": 5,
                "shift": 15,
                "w": 4,
                "x": 8,
                "y": 147
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "5251c5a6-758d-4b1e-bf60-5199937cdb73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 27,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 239,
                "y": 89
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "60fe60ad-f3fc-472c-b198-9f98f3a1eb51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 16,
                "y": 31
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "33a9faa6-fa5f-429a-a6c1-b2c218b6a2eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 27,
                "offset": 0,
                "shift": 15,
                "w": 12,
                "x": 198,
                "y": 89
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 20,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}