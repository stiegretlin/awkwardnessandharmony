/// @description update camera
//Update destination
if (instance_exists(follow))
{
	xTo = follow.x;
	yTo = follow.y;
}

//Update object position
x += (xTo - x) / 25;
y += (yTo - y) / 25;

x = clamp(x,view_width_half,room_width-view_width_half);
y = clamp(y,view_height_half,room_height-view_height_half);

//Update camera view
camera_set_view_pos(cam,x-view_width_half,y-view_height_half);
