{
    "id": "3aac6362-4396-46c3-82ca-1b25544070c8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_sounds",
    "eventList": [
        {
            "id": "0c28dc01-2b78-4b31-a77c-075217686d28",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3aac6362-4396-46c3-82ca-1b25544070c8"
        },
        {
            "id": "e2d1876a-7d73-4715-8580-2ca488f8d4e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 72,
            "eventtype": 7,
            "m_owner": "3aac6362-4396-46c3-82ca-1b25544070c8"
        },
        {
            "id": "26bb2fb3-0c83-4675-992d-2d1ae314266c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3aac6362-4396-46c3-82ca-1b25544070c8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}