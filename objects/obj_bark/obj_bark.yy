{
    "id": "d2f20380-559f-44d9-aaa6-27d9f726a492",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bark",
    "eventList": [
        {
            "id": "f62119b9-37ff-401b-9866-809ea3b5111e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d2f20380-559f-44d9-aaa6-27d9f726a492"
        },
        {
            "id": "cff38077-598d-4ab8-9039-e1513fc0dbb6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d2f20380-559f-44d9-aaa6-27d9f726a492"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2d1b4923-736e-4261-bbbc-24e62e42db2a",
    "visible": true
}