/// @description menu step action

var move_cursor = 0;
move_cursor -= max(keyboard_check_pressed(vk_up),(keyboard_check_pressed(ord("W"))), gamepad_button_check_pressed(0, gp_padu),0);
move_cursor += max(keyboard_check_pressed(vk_down),(keyboard_check_pressed(ord("S"))), gamepad_button_check_pressed(0, gp_padd),0);

if (move_cursor != 0)
{
	menu_cursor += move_cursor;
	if (menu_cursor < 0) menu_cursor = array_length_1d(menu) - 1;
	if (menu_cursor > array_length_1d(menu) - 1) menu_cursor = 0;
}

var select_cursor;
select_cursor = max(keyboard_check_released(ord("Z")),keyboard_check_released(vk_shift),keyboard_check_released(vk_enter), gamepad_button_check_released(0, gp_face1), 0);
if (select_cursor == 1) scr_menu();

#region //sound lol
if (move_cursor < 0) audio_play_sound(snd_happy,1,false);
if (move_cursor > 0) audio_play_sound(snd_sad,1,false);
if (select_cursor == 1) audio_play_sound(snd_happy,1,false);
#endregion