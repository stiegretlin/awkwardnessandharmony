/// @description abby init

#region //movement

speed_horiz = 0;
speed_vert = 0;
speed_walk = 5;
controller = 0;
hascontrol = true;
abby_mood = 4;
abby_ai = "walk_left";
move_horiz = 0;
move_vert = 0;
collidables = (obj_wall_abby);
collidapad = 0;
bark_collide = false;
mood_change = false;

#endregion

#region // sprite stuff


abby_head = instance_create_layer(x, y, "inst_abby_head", obj_abby_head);

#endregion