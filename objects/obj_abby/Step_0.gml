/// @description abby behavior

#region //pause function
if (global.pause) exit;
#endregion

#region //move

//horiz move
x = x + speed_horiz;

//vert move
y = y + speed_vert;

#endregion

#region //movement logic
switch (abby_ai)
{
	case "walk_left":
		speed_horiz = -1;
	break;
	
	case "walk_right":
		speed_horiz = 1;
	break;
	
}
#endregion


#region //collision



//horiz collision
if (place_meeting(x+speed_horiz+collidapad,y,collidables))
{
	while (!place_meeting(x+sign(speed_horiz)+collidapad,y,collidables))
	{
		x = x + sign(speed_horiz);
	}
	//speed_horiz = 0;
	if (abby_ai = "walk_left" )
	{
		abby_ai = "walk_right";
			
		abby_mood -= 1;
		abby_mood = clamp(abby_mood,1,5) - 1;
			instance_create_layer(x,y,"inst_speech",obj_react);
			audio_play_sound(snd_bump,2,false) audio_sound_pitch(snd_bump, random_range(-.1, 1));
			
	} else if (abby_ai = "walk_right" )
	{
		abby_ai = "walk_left";
				abby_mood -= 1;
				abby_mood = clamp(abby_mood,1,5) - 1;
					instance_create_layer(x,y,"inst_speech",obj_react);
					audio_play_sound(snd_bump,2,false) audio_sound_pitch(snd_bump, random_range(-1, 1));
	}
}

//Vertical Collision
if (place_meeting(x,y+speed_vert+collidapad,collidables))
{
	while (!place_meeting(x,y+sign(speed_vert)+collidapad,collidables))
	{
		y = y + sign(speed_vert);
	}
	speed_vert = 0;
}
#endregion

#region //attach head
abby_head.x = x;
abby_head.y = y;
#endregion

#region //animate walk

if (speed_horiz = 0) && (speed_vert = 0)
{
	obj_abby.sprite_index = spr_abby_body_still;
	obj_abby.image_speed = 1;
}
else if (speed_horiz >0) || (speed_horiz < 0)
{
	obj_abby.sprite_index = spr_abby_body_walk;
	obj_abby.image_speed = 1;
	obj_abby.image_xscale = sign(speed_horiz);
	abby_head.image_xscale = sign(speed_horiz);
}
else if (speed_vert > 0) || (speed_vert < 0)
{
	obj_abby.sprite_index = spr_abby_body_walk;
	obj_abby.image_speed = 1;
	obj_abby.image_xscale = sign(speed_vert);
	abby_head.image_xscale = sign(speed_vert);
}

#endregion

#region //art flip horiz

if (speed_horiz < 0)
{
	abby_head.image_xscale = -1;	
}
else if (speed_horiz > 0)
{
	abby_head.image_xscale = 1;
}

//image_xscale = sign(move_horiz)

#endregion

#region //mood management

obj_abby_head.image_index = abby_mood;

/*
if bark_collide = false && (place_meeting(x,y,obj_bark))
{
		mood_change = true;
		bark_collide = true;
	} 
	// if bark_collide = true && (!place_meeting(x,y,obj_bark))
	else
	{
		bark_collide = false;
		mood_change = false;
}


if (mood_change = true) // && (global.abby_mood <= global.player_mood)
{
	if (abby_mood >= 0) && (abby_mood <= 4) && (abby_mood < global.player_mood)
	{
	abby_mood +=1;
	}
	//global.player_mood -=1;
}
*/


//thankyou knappi for this
if (bark_collide = true && place_meeting(x,y,obj_bark) = true) {
    abby_mood += 1; 
	clamp(abby_mood,1,5);
   global.player_mood -= 1;
   clamp(global.player_mood,1,5);
    bark_collide = false;
	instance_create_layer(x,y,"inst_speech",obj_react);
}

if (place_meeting(x,y,obj_bark) = false) {
    bark_collide = true;
}

#endregion
