{
    "id": "fd2a1020-8e78-4e27-b886-08d398bbb496",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_abby",
    "eventList": [
        {
            "id": "d9ca76e5-c629-4e4d-83b9-a75790ad14a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fd2a1020-8e78-4e27-b886-08d398bbb496"
        },
        {
            "id": "30bc5169-9feb-4e81-96d5-03406d2be498",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fd2a1020-8e78-4e27-b886-08d398bbb496"
        },
        {
            "id": "8ff5b22d-cc0e-4aab-b3a9-4cdabb9eef7d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "fd2a1020-8e78-4e27-b886-08d398bbb496"
        }
    ],
    "maskSpriteId": "cb55f791-3a85-4d82-a85c-91dcce3a4849",
    "overriddenProperties": null,
    "parentObjectId": "65ff0347-2e34-491e-9a90-cd85359d4e68",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a7b5df2e-1980-4dbc-93f1-5faf35bec8b4",
    "visible": true
}