/// @description kitty behavior

#region //pause function
if (global.pause) exit;
#endregion

#region //move

//horiz move
x = x + speed_horiz;

//vert move
y = y + speed_vert;

#endregion

#region //movement logic
switch (kitty_ai)
{
	case "walk_left":
		speed_horiz = -1;
	break;
	
	case "walk_right":
		speed_horiz = 1;
	break;
	
}
#endregion

script_execute(scr_collision);

#region //collision



//horiz collision
if (place_meeting(x+speed_horiz+collidapad,y,collidables))
{
	while (!place_meeting(x+sign(speed_horiz)+collidapad,y,collidables))
	{
		x = x + sign(speed_horiz);
	}
	//speed_horiz = 0;
	if (kitty_ai = "walk_left" )
	{
		kitty_ai = "walk_right";
								mood -= 1;
				mood = clamp(mood,1,5) - 1;
					instance_create_layer(x,y,"inst_speech",obj_react);
					audio_play_sound(snd_bump,2,false) audio_sound_pitch(snd_bump, random_range(-.1, 1));
	} else if (kitty_ai = "walk_right" )
	{
		kitty_ai = "walk_left";
						mood -= 1;
				mood = clamp(mood,1,5) - 1;
					instance_create_layer(x,y,"inst_speech",obj_react);
					audio_play_sound(snd_bump,2,false) audio_sound_pitch(snd_bump, random_range(-.1, 1));
	}
}

//Vertical Collision
if (place_meeting(x,y+speed_vert+collidapad,collidables))
{
	while (!place_meeting(x,y+sign(speed_vert)+collidapad,collidables))
	{
		y = y + sign(speed_vert);
	}
	speed_vert = 0;
}
#endregion

#region //attach head
head.x = x;
head.y = y;
#endregion

#region //animate walk

if (speed_horiz = 0) && (speed_vert = 0)
{
	obj_kitty.sprite_index = spr_kitty_body_walk;
	obj_kitty.image_speed = 0;
}
else if (speed_horiz >0) || (speed_horiz < 0)
{
	obj_kitty.sprite_index = spr_kitty_body_walk;
	obj_kitty.image_speed = 1;
	obj_kitty.image_xscale = sign(speed_horiz);
	head.image_xscale = sign(speed_horiz);
}
else if (speed_vert > 0) || (speed_vert < 0)
{
	obj_kitty.sprite_index = spr_kitty_body_walk;
	obj_kitty.image_speed = 1;
	obj_kitty.image_xscale = sign(speed_vert);
	head.image_xscale = sign(speed_vert);
}

#endregion

#region //art flip horiz

if (speed_horiz < 0)
{
	head.image_xscale = -1;	
}
else if (speed_horiz > 0)
{
	head.image_xscale = 1;
}

//image_xscale = sign(move_horiz)

#endregion

#region //mood management

obj_kitty_head.image_index = mood;


//thankyou knappi for this
if (bark_collide = true && place_meeting(x,y,obj_bark) = true) {
    mood +=1;
   global.player_mood -=1;
    bark_collide = false;
	instance_create_layer(x,y,"inst_speech",obj_react);
}

if (place_meeting(x,y,obj_bark) = false) {
    bark_collide = true;
}

#endregion
