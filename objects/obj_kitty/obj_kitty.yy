{
    "id": "011a2d02-e1c9-464d-9668-fcea4180d8f4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_kitty",
    "eventList": [
        {
            "id": "4b6f0ee1-7303-43c0-b846-e3e586af7e1f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "011a2d02-e1c9-464d-9668-fcea4180d8f4"
        },
        {
            "id": "d958ec54-1322-41d3-984f-506983bb26d5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "011a2d02-e1c9-464d-9668-fcea4180d8f4"
        },
        {
            "id": "a88e9863-e4f3-4121-bf76-1fccdad8da39",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "011a2d02-e1c9-464d-9668-fcea4180d8f4"
        }
    ],
    "maskSpriteId": "cb55f791-3a85-4d82-a85c-91dcce3a4849",
    "overriddenProperties": null,
    "parentObjectId": "65ff0347-2e34-491e-9a90-cd85359d4e68",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "77c99071-8863-4e38-b584-f2545ad0420d",
    "visible": true
}