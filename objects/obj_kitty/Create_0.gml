/// @description kitty init

speed_horiz = 0;
speed_vert = 0;
speed_walk = 5;
controller = 0;
hascontrol = true;
mood = 5;
kitty_ai = "walk_right";
move_horiz = 0;
move_vert = 0;
collidables = (obj_wall_abby);
collidapad = 0;
bark_collide = false;
mood_change = false;

/*
key_left = global.key_left;
key_right = global.key_right;
key_down = global.key_down;
key_right = global.key_right;
key_accept = global.key_accept;
key_pause = global.key_pause;
*/


head = instance_create_layer(x, y, "inst_abby_head", obj_kitty_head);
