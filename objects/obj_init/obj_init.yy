{
    "id": "506f78ee-e738-4feb-8762-53a7dff6700a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_init",
    "eventList": [
        {
            "id": "7ae27f3e-2e61-4c86-bffe-04cabab0831d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "506f78ee-e738-4feb-8762-53a7dff6700a"
        },
        {
            "id": "79bb3a98-4be0-4eb5-a3ec-aae6aade495f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "506f78ee-e738-4feb-8762-53a7dff6700a"
        },
        {
            "id": "939448ed-36ac-47de-8dca-54be31684d7e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "506f78ee-e738-4feb-8762-53a7dff6700a"
        },
        {
            "id": "fb321046-2b2c-415e-82d6-a9add7b979e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 72,
            "eventtype": 7,
            "m_owner": "506f78ee-e738-4feb-8762-53a7dff6700a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}