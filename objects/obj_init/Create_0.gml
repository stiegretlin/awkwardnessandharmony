/// @description global variables

global.pause = false;
global.fullscreen = false;
global.bgmusic = true;

audio_group_load(audiogroup_soundfx);
audio_group_load(audiogroup_music);

global.sosmessage = "";
// global.sosmessage = "   help I am trapped in a menu";
