/// @description global logic, menus, etc

#region //room control
if room = rm_init
 {
room_goto(rm_menu)
 }
#endregion
 
#region //pause switch
	key_pause = max(keyboard_check_pressed(vk_escape), keyboard_check_pressed(vk_enter), gamepad_button_check_pressed(0, gp_start), 0);
if room = (rm_level)
{
	if (key_pause = 1)&&(global.pause == 0)
	{
		global.pause = 1;
		instance_create_layer(200, 321, "inst_menu", obj_menu);
	}
	else if (key_pause = 1)&&(global.pause == 1)
	{
		global.pause = 0;
		instance_destroy(obj_menu);
		instance_destroy(obj_help);
	}
}
#endregion

#region //fullscreen

	key_fullscreen = max(keyboard_check_pressed(ord("F")), 0);

	if (key_fullscreen = 1)&&(global.fullscreen == 0)
	{
		global.fullscreen = 1;
		window_set_fullscreen(true);
	}
	else if (key_fullscreen = 1)&&(global.fullscreen = 1)
	{
		global.fullscreen = 0;
		window_set_fullscreen(false);
	}
#endregion