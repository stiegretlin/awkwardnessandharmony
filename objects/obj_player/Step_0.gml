/// @description player behavior

script_execute(scr_input);

#region //Input Movements

//left right movement
var move_horiz = key_right - key_left;
speed_horiz = move_horiz * speed_walk;

//up down movement
var move_vert = key_down - key_up;
speed_vert = move_vert * speed_walk;

#endregion

#region //Emit speech new

if (key_accept = 1)
	{
		instance_create_layer(x,y,"inst_speech",obj_bark);
		audio_play_sound(snd_happy,1,false);
	}

#endregion

script_execute(scr_collision);

#region //attach head
player_head.x = x;
player_head.y = y;
#endregion

#region //animate walk

if (move_horiz = 0) && (move_vert = 0)
{
	obj_player.sprite_index = spr_fred_body_still;
	obj_player.image_speed = 1;
}
else if (move_horiz >0) || (move_horiz < 0)
{
	obj_player.sprite_index = spr_fred_body_walk;
	obj_player.image_speed = 1;
	obj_player.image_xscale = sign(move_horiz);
	player_head.image_xscale = sign(move_horiz);
}
else if (move_vert > 0) || (move_vert < 0)
{
	obj_player.sprite_index = spr_fred_body_walk;
	obj_player.image_speed = 1;
	obj_player.image_xscale = sign(move_vert);
	player_head.image_xscale = sign(move_vert);
}

#endregion

#region //art flip horiz
/*
if (move_horiz < 0)
{
	player_head.image_xscale = -1;	
}
else if (move_horiz > 0)
{
	player_head.image_xscale = 1;
}
*/
//image_xscale = sign(move_horiz)

#endregion

#region //mood management

obj_player_head.image_index = global.player_mood;

#endregion
