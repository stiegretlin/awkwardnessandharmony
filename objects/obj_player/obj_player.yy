{
    "id": "887fca44-311e-4806-9fe3-a40f737cf1fd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "21eb8819-e3ea-4cfd-baab-700804a22404",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "887fca44-311e-4806-9fe3-a40f737cf1fd"
        },
        {
            "id": "0b170d64-a679-4867-92b5-a37d8008c876",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "887fca44-311e-4806-9fe3-a40f737cf1fd"
        },
        {
            "id": "fd9ec705-6a87-482e-ace8-00cdb5549196",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "887fca44-311e-4806-9fe3-a40f737cf1fd"
        }
    ],
    "maskSpriteId": "cb55f791-3a85-4d82-a85c-91dcce3a4849",
    "overriddenProperties": null,
    "parentObjectId": "196684b7-11bf-475f-98d1-bde1d7b93e4a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "db0644f6-bc2f-403d-b791-0b10b8c71fe3",
    "visible": true
}