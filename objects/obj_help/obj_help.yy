{
    "id": "f4dd1b26-ce1f-4adb-bc4e-a7e57019449c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_help",
    "eventList": [
        {
            "id": "9e448bf6-0a80-4f98-813d-dffeb82f4138",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f4dd1b26-ce1f-4adb-bc4e-a7e57019449c"
        },
        {
            "id": "33bf5aa1-c020-43ca-b9ac-070f732a6120",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f4dd1b26-ce1f-4adb-bc4e-a7e57019449c"
        },
        {
            "id": "7f48df28-7124-4f90-8649-493e930b708d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f4dd1b26-ce1f-4adb-bc4e-a7e57019449c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "55e3e61b-7114-4df0-902f-37c7b0c3a1c5",
    "visible": true
}