/// @description menu options

menu[0] = "Goal:         Speak to improve mood";
menu[1] = "Move:         D-Pad or WASD gamepad +";
menu[2] = "Speak:        Z or Shift or gamepad A";
menu[3] = global.sosmessage;
menu[4] = "Back to Menu";

menu_leading = 32;
menu_cursor = 0;