{
    "id": "4b13e592-d0b1-41aa-b01e-8f8f751a1990",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_react",
    "eventList": [
        {
            "id": "04295357-72cf-4daf-abc8-f05c9875b11a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4b13e592-d0b1-41aa-b01e-8f8f751a1990"
        },
        {
            "id": "1e986be2-d1d4-418e-b789-d8abd98c030f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4b13e592-d0b1-41aa-b01e-8f8f751a1990"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0aae60dc-4fd1-4b34-966e-7f43e557b33e",
    "visible": true
}