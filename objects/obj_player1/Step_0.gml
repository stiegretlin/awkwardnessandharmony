/// @description player behavior

#region //pause function
if (global.pause) exit;
#endregion

#region //Get Player Input
if (hascontrol)
{
	
	key_left = max (keyboard_check(vk_left), keyboard_check(ord("A")), gamepad_button_check(0, gp_padl), 0);
	key_right = max (keyboard_check(vk_right), keyboard_check(ord("D")), gamepad_button_check(0, gp_padr), 0);
	key_down = max (keyboard_check(vk_down), keyboard_check(ord("S")), gamepad_button_check(0, gp_padd), 0);
	key_up = max (keyboard_check(vk_up), keyboard_check(ord("W")), gamepad_button_check(0, gp_padu), 0);
	
	key_accept = max (keyboard_check_pressed(ord("Z")), keyboard_check_pressed(ord("J")), gamepad_button_check_pressed(0, gp_face1), 0);
	// key_decline = max (keyboard_check_pressed(ord("X")), keyboard_check_pressed(ord("K")), 0);
	
}
else 
{
	key_left = 0;
	key_right = 0;
	key_down = 0;
	key_up = 0;
	
	key_accept = 0;
	// key_decline = 0;
}
#endregion

#region //Input Movements

//left right movement
var move_horiz = key_right - key_left;
speed_horiz = move_horiz * speed_walk;

//up down movement
var move_vert = key_down - key_up;
speed_vert = move_vert * speed_walk;

#endregion

#region //Emit speech new

if (key_accept = 1)
	{
		instance_create_layer(x,y,"inst_speech",obj_bark);
		audio_play_sound(snd_happy,1,false);
	}

#endregion

#region //Emit speech old
/*
//if (global.player_mood >= 0)&&(global.player_mood <= 4)
{
	if (key_accept = 1)
	{
		global.player_mood += 1;
	}
	if (key_decline = 1)
	{
		global.player_mood -= 1;
	}
}	
*/
#endregion

#region //collision check and move



//horiz collision
if (place_meeting(x+speed_horiz+collidapad,y,collidables))
{
	while (!place_meeting(x+sign(speed_horiz)+collidapad,y,collidables))
	{
		x = x + sign(speed_horiz);
	}
	speed_horiz = 0;
}

//horiz move
x = x + speed_horiz;

//Vertical Collision
if (place_meeting(x,y+speed_vert+collidapad,collidables))
{
	while (!place_meeting(x,y+sign(speed_vert)+collidapad,collidables))
	{
		y = y + sign(speed_vert);
	}
	speed_vert = 0;
}
//verti move
y = y + speed_vert;

#endregion

#region //attach head
player_head.x = x;
player_head.y = y;
#endregion

#region //animate walk

if (move_horiz = 0) && (move_vert = 0)
{
	obj_player.sprite_index = spr_fred_body_still;
	obj_player.image_speed = 1;
}
else if (move_horiz >0) || (move_horiz < 0)
{
	obj_player.sprite_index = spr_fred_body_walk;
	obj_player.image_speed = 1;
	obj_player.image_xscale = sign(move_horiz);
	player_head.image_xscale = sign(move_horiz);
}
else if (move_vert > 0) || (move_vert < 0)
{
	obj_player.sprite_index = spr_fred_body_walk;
	obj_player.image_speed = 1;
	obj_player.image_xscale = sign(move_vert);
	player_head.image_xscale = sign(move_vert);
}

#endregion

#region //art flip horiz
/*
if (move_horiz < 0)
{
	player_head.image_xscale = -1;	
}
else if (move_horiz > 0)
{
	player_head.image_xscale = 1;
}
*/
//image_xscale = sign(move_horiz)

#endregion

#region //mood management

obj_player_head.image_index = global.player_mood;

#endregion
