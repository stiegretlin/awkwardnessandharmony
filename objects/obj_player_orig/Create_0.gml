/// @description player init

speed_horiz = 0;
speed_vert = 0;
speed_walk = 5;
controller = 0;
hascontrol = true;
global.player_mood = 4;
collidables = max(obj_wall);
collidapad = 0;

player_head = instance_create_layer(x, y, "inst_player_head", obj_player_head);
