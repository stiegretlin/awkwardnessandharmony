{
    "id": "c5d057a9-d1c0-4af4-976c-1e3bd34c06b1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player_orig",
    "eventList": [
        {
            "id": "3fdc1f2c-f751-4adc-a46f-447f9ef9edbe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c5d057a9-d1c0-4af4-976c-1e3bd34c06b1"
        },
        {
            "id": "422eafdf-ceff-4909-b409-4ce40e13b8f4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c5d057a9-d1c0-4af4-976c-1e3bd34c06b1"
        },
        {
            "id": "641f9081-2b77-411e-a950-b656087a70aa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c5d057a9-d1c0-4af4-976c-1e3bd34c06b1"
        }
    ],
    "maskSpriteId": "cb55f791-3a85-4d82-a85c-91dcce3a4849",
    "overriddenProperties": null,
    "parentObjectId": "196684b7-11bf-475f-98d1-bde1d7b93e4a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "db0644f6-bc2f-403d-b791-0b10b8c71fe3",
    "visible": true
}