///script input

#region //pause function
if (global.pause) exit;
#endregion

#region //Get Player Input
if (hascontrol)
{
	
	key_left = max (keyboard_check(vk_left), keyboard_check(ord("A")), gamepad_button_check(0, gp_padl), 0);
	key_right = max (keyboard_check(vk_right), keyboard_check(ord("D")), gamepad_button_check(0, gp_padr), 0);
	key_down = max (keyboard_check(vk_down), keyboard_check(ord("S")), gamepad_button_check(0, gp_padd), 0);
	key_up = max (keyboard_check(vk_up), keyboard_check(ord("W")), gamepad_button_check(0, gp_padu), 0);
	
	key_accept = max (keyboard_check_pressed(ord("Z")), keyboard_check_pressed(ord("J")), gamepad_button_check_pressed(0, gp_face1), 0);
	// key_decline = max (keyboard_check_pressed(ord("X")), keyboard_check_pressed(ord("K")), 0);
	
}
else 
{
	key_left = 0;
	key_right = 0;
	key_down = 0;
	key_up = 0;
	
	key_accept = 0;
	// key_decline = 0;
}
#endregion