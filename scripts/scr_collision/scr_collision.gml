///script collisions


#region //collision check and move

//horiz collision
if (place_meeting(x+speed_horiz+collidapad,y,collidables))
{
	while (!place_meeting(x+sign(speed_horiz)+collidapad,y,collidables))
	{
		x = x + sign(speed_horiz);
	}
	speed_horiz = 0;
}

//horiz move
x = x + speed_horiz;

//Vertical Collision
if (place_meeting(x,y+speed_vert+collidapad,collidables))
{
	while (!place_meeting(x,y+sign(speed_vert)+collidapad,collidables))
	{
		y = y + sign(speed_vert);
	}
	speed_vert = 0;
}
//verti move
y = y + speed_vert;

#endregion
