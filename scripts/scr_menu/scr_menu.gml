///scr_menu

switch (menu_cursor)
{
	case 0: //start game
	{
		room_goto(rm_level);
		global.pause = 0;
	}

	case 1: //go to help menu
	{
		audio_play_sound(snd_happy,1,false);
		instance_change(obj_help, true);
		break;
	}
	
	case 2: //mute unmute bgmusic
	{
		switch global.bgmusic
		{
			case true:
				global.bgmusic = false;
				audio_pause_sound(snd_music);
				break;
			case false:
				global.bgmusic = true;
				audio_play_sound(snd_music,10,true);
				break;
		}

		/*
		if (global.bgmusic == true)
		{
			global.bgmusic == false;
			audio_pause_sound(snd_music);
		}
		else if (global.bgmusic == false)
		{
			global.bgmusic == true;
			audio_play_sound(snd_music,10,true);
		}
		*/
	}
	
	case 3: //fullscreen switcher
	{
		/*
		switch global.fullscreen
		{
			case false:
				global.fullscreen = true;
				window_set_fullscreen(true);
				break;
			case true:
				global.fullscreen = false;
				window_set_fullscreen(false);
				break;
		}
		*/

				if (global.fullscreen == 0)
			{
				global.fullscreen = 1;
				window_set_fullscreen(true);
			}
			 if (global.fullscreen == 1)
			{
				global.fullscreen = 0;
				window_set_fullscreen(false);
			}
	break;
	}
	
	case 4: //quit game
	{
		game_end();
		break;
	}
						
	default: break;
}