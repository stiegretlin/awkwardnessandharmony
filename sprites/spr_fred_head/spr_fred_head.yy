{
    "id": "c6ac31ad-7a5c-4582-ab1d-78a6b54eed57",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fred_head",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 13,
    "bbox_right": 49,
    "bbox_top": 17,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6e3aec59-9594-411b-9009-826ed9bbc8c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6ac31ad-7a5c-4582-ab1d-78a6b54eed57",
            "compositeImage": {
                "id": "3cf8c6e6-a502-431c-8738-c173ebdabe1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e3aec59-9594-411b-9009-826ed9bbc8c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "461ec48e-9fe4-4aba-881b-94b303bb1c1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e3aec59-9594-411b-9009-826ed9bbc8c7",
                    "LayerId": "f8c1205c-e0dc-4c91-91e7-d8eacfd2c83c"
                }
            ]
        },
        {
            "id": "362e0ce8-b322-47d9-8b17-f49bb2bb795d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6ac31ad-7a5c-4582-ab1d-78a6b54eed57",
            "compositeImage": {
                "id": "063c76ff-89cc-4522-947e-9eb6cd95707f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "362e0ce8-b322-47d9-8b17-f49bb2bb795d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c224352c-eeb1-45ae-ad23-08e2bd0beda6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "362e0ce8-b322-47d9-8b17-f49bb2bb795d",
                    "LayerId": "f8c1205c-e0dc-4c91-91e7-d8eacfd2c83c"
                }
            ]
        },
        {
            "id": "ec912a8f-25ad-4079-bab5-4c93654e9634",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6ac31ad-7a5c-4582-ab1d-78a6b54eed57",
            "compositeImage": {
                "id": "fd6cad0e-a7ec-46f2-b677-edb854bba46d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec912a8f-25ad-4079-bab5-4c93654e9634",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b643b2f-cd79-4fc1-935a-0c8d386862c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec912a8f-25ad-4079-bab5-4c93654e9634",
                    "LayerId": "f8c1205c-e0dc-4c91-91e7-d8eacfd2c83c"
                }
            ]
        },
        {
            "id": "12ea494e-a006-47a9-b9a8-7c4b2b1e4f75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6ac31ad-7a5c-4582-ab1d-78a6b54eed57",
            "compositeImage": {
                "id": "123b9911-7316-48f4-8c92-e61428d5f56a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12ea494e-a006-47a9-b9a8-7c4b2b1e4f75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54c94c60-cf34-44c8-8da8-250eb45f32d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12ea494e-a006-47a9-b9a8-7c4b2b1e4f75",
                    "LayerId": "f8c1205c-e0dc-4c91-91e7-d8eacfd2c83c"
                }
            ]
        },
        {
            "id": "5ab7fd96-bbb0-458e-966f-251e07582bf1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6ac31ad-7a5c-4582-ab1d-78a6b54eed57",
            "compositeImage": {
                "id": "b7e09ded-68a7-4fb6-b692-4aff09005e92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ab7fd96-bbb0-458e-966f-251e07582bf1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d2b4a7b-5fa9-4642-9250-790cc2e47a54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ab7fd96-bbb0-458e-966f-251e07582bf1",
                    "LayerId": "f8c1205c-e0dc-4c91-91e7-d8eacfd2c83c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f8c1205c-e0dc-4c91-91e7-d8eacfd2c83c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c6ac31ad-7a5c-4582-ab1d-78a6b54eed57",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 127
}