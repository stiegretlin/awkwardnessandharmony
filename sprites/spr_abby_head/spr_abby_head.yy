{
    "id": "4db1faca-3e3e-4324-bbad-aa2dc852f601",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_abby_head",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 16,
    "bbox_right": 54,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "10a6a16f-90bc-4f1e-8451-43ea846e1817",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4db1faca-3e3e-4324-bbad-aa2dc852f601",
            "compositeImage": {
                "id": "6a8882f7-822e-4613-a0c8-e40dbfd5489a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10a6a16f-90bc-4f1e-8451-43ea846e1817",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9d47277-c23a-47ed-9977-fbe3cecf9a1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10a6a16f-90bc-4f1e-8451-43ea846e1817",
                    "LayerId": "ac928ae1-db38-4178-9851-6e2ac79a33d1"
                }
            ]
        },
        {
            "id": "7e386ab6-4982-4a60-9d22-99ff833aab97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4db1faca-3e3e-4324-bbad-aa2dc852f601",
            "compositeImage": {
                "id": "5ff8367a-70b6-49d7-8bc5-e45d9a7c623f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e386ab6-4982-4a60-9d22-99ff833aab97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d12097ba-987c-4507-8828-fa65c0376654",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e386ab6-4982-4a60-9d22-99ff833aab97",
                    "LayerId": "ac928ae1-db38-4178-9851-6e2ac79a33d1"
                }
            ]
        },
        {
            "id": "6e669960-f560-40d7-9232-24af33f4fa68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4db1faca-3e3e-4324-bbad-aa2dc852f601",
            "compositeImage": {
                "id": "96c96e3b-4efc-486b-b888-898de605bbff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e669960-f560-40d7-9232-24af33f4fa68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "164a4de3-a2d4-41a7-a267-076527e65996",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e669960-f560-40d7-9232-24af33f4fa68",
                    "LayerId": "ac928ae1-db38-4178-9851-6e2ac79a33d1"
                }
            ]
        },
        {
            "id": "8f4db678-2597-45ae-a030-ac073b0a2180",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4db1faca-3e3e-4324-bbad-aa2dc852f601",
            "compositeImage": {
                "id": "eb257a77-fd51-441c-af38-cd8eb48e7e87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f4db678-2597-45ae-a030-ac073b0a2180",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "487e9f51-8ac5-4da6-b5cd-844e079fae62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f4db678-2597-45ae-a030-ac073b0a2180",
                    "LayerId": "ac928ae1-db38-4178-9851-6e2ac79a33d1"
                }
            ]
        },
        {
            "id": "bc9323fc-2362-44d8-b5b4-64278d3c39c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4db1faca-3e3e-4324-bbad-aa2dc852f601",
            "compositeImage": {
                "id": "8340a821-aa7a-4108-bba4-29db964b438d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc9323fc-2362-44d8-b5b4-64278d3c39c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c31a54f2-39a8-454c-94d5-631de63b44ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc9323fc-2362-44d8-b5b4-64278d3c39c2",
                    "LayerId": "ac928ae1-db38-4178-9851-6e2ac79a33d1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 70,
    "layers": [
        {
            "id": "ac928ae1-db38-4178-9851-6e2ac79a33d1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4db1faca-3e3e-4324-bbad-aa2dc852f601",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 33,
    "yorig": 127
}