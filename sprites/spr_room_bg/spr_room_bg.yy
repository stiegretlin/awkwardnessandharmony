{
    "id": "67d4bad5-6d32-42e9-bad2-537e44436c23",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_room_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 539,
    "bbox_left": 0,
    "bbox_right": 959,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "97278230-a47b-4093-b5e7-dfbfc841ac84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67d4bad5-6d32-42e9-bad2-537e44436c23",
            "compositeImage": {
                "id": "a0210380-f739-4ede-b0f4-979292a7aa0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97278230-a47b-4093-b5e7-dfbfc841ac84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74736ebb-e5ec-4154-9a02-0701cc024390",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97278230-a47b-4093-b5e7-dfbfc841ac84",
                    "LayerId": "8441b020-4c67-4dce-857d-02dfa018c6ff"
                },
                {
                    "id": "da2e08d9-40cc-47af-bf46-fc449c2e6ec6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97278230-a47b-4093-b5e7-dfbfc841ac84",
                    "LayerId": "4510388c-8970-4f6e-8f98-046c05335560"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 540,
    "layers": [
        {
            "id": "8441b020-4c67-4dce-857d-02dfa018c6ff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "67d4bad5-6d32-42e9-bad2-537e44436c23",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "4510388c-8970-4f6e-8f98-046c05335560",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "67d4bad5-6d32-42e9-bad2-537e44436c23",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 960,
    "xorig": 0,
    "yorig": 0
}