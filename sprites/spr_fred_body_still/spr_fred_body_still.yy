{
    "id": "cb55f791-3a85-4d82-a85c-91dcce3a4849",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fred_body_still",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 4,
    "bbox_right": 60,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f921e4e5-ee14-446a-94b8-3ae88bb60fb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb55f791-3a85-4d82-a85c-91dcce3a4849",
            "compositeImage": {
                "id": "92ca5fe4-f389-44c2-accc-f3cdcd5bc052",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f921e4e5-ee14-446a-94b8-3ae88bb60fb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "952cce4c-1dc5-496d-a3dc-43d06190c763",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f921e4e5-ee14-446a-94b8-3ae88bb60fb3",
                    "LayerId": "4e808826-69d9-42b4-a328-9fa0cf83517a"
                },
                {
                    "id": "75c1e43a-5a83-4d5b-94ec-1af8f30f4789",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f921e4e5-ee14-446a-94b8-3ae88bb60fb3",
                    "LayerId": "de57dea8-f21b-4c65-94ee-383a5c57f284"
                }
            ]
        },
        {
            "id": "5a2c411e-3163-4645-a12d-f3b2105b42c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb55f791-3a85-4d82-a85c-91dcce3a4849",
            "compositeImage": {
                "id": "4422dea4-d6ab-4cbb-80d5-7ae084614601",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a2c411e-3163-4645-a12d-f3b2105b42c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19b02fb6-9659-454c-9256-be391930773f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a2c411e-3163-4645-a12d-f3b2105b42c5",
                    "LayerId": "de57dea8-f21b-4c65-94ee-383a5c57f284"
                },
                {
                    "id": "4dfcbd1b-a05b-445e-9bc9-c2c3a134c0c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a2c411e-3163-4645-a12d-f3b2105b42c5",
                    "LayerId": "4e808826-69d9-42b4-a328-9fa0cf83517a"
                }
            ]
        },
        {
            "id": "6a850c94-d957-4e43-9681-1f7ec8c7b69f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb55f791-3a85-4d82-a85c-91dcce3a4849",
            "compositeImage": {
                "id": "c1143b7d-91b1-483b-a065-78bffcf0e993",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a850c94-d957-4e43-9681-1f7ec8c7b69f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59873ef1-0f1a-4d0a-8f5c-ee17075b2c6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a850c94-d957-4e43-9681-1f7ec8c7b69f",
                    "LayerId": "de57dea8-f21b-4c65-94ee-383a5c57f284"
                },
                {
                    "id": "e0f07000-5d6a-4b13-a8b7-93686cdb1279",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a850c94-d957-4e43-9681-1f7ec8c7b69f",
                    "LayerId": "4e808826-69d9-42b4-a328-9fa0cf83517a"
                }
            ]
        },
        {
            "id": "22c7f11f-2a07-4f88-b3dc-ad9769a8f4cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb55f791-3a85-4d82-a85c-91dcce3a4849",
            "compositeImage": {
                "id": "1f489968-2733-4af9-872d-cd5b4add1f4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22c7f11f-2a07-4f88-b3dc-ad9769a8f4cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33db90c5-37e1-46b5-bca6-cab953bb6197",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22c7f11f-2a07-4f88-b3dc-ad9769a8f4cd",
                    "LayerId": "de57dea8-f21b-4c65-94ee-383a5c57f284"
                },
                {
                    "id": "bfbdcd72-0b5f-42a7-8ec2-cd13f59fbd54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22c7f11f-2a07-4f88-b3dc-ad9769a8f4cd",
                    "LayerId": "4e808826-69d9-42b4-a328-9fa0cf83517a"
                }
            ]
        },
        {
            "id": "2a1ab109-887f-4ab3-b14e-69f25c084ffd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb55f791-3a85-4d82-a85c-91dcce3a4849",
            "compositeImage": {
                "id": "72dd18ec-4b15-4e6a-9668-aa054c601cca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a1ab109-887f-4ab3-b14e-69f25c084ffd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8122117c-4204-4471-8c5b-d8208399158e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a1ab109-887f-4ab3-b14e-69f25c084ffd",
                    "LayerId": "de57dea8-f21b-4c65-94ee-383a5c57f284"
                },
                {
                    "id": "3c647ee3-02cd-405a-8c67-7ff1eefcd7ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a1ab109-887f-4ab3-b14e-69f25c084ffd",
                    "LayerId": "4e808826-69d9-42b4-a328-9fa0cf83517a"
                }
            ]
        },
        {
            "id": "45263a6d-d817-4140-be69-9f18aca9505a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb55f791-3a85-4d82-a85c-91dcce3a4849",
            "compositeImage": {
                "id": "80f01eae-daa2-4700-a4a2-903ae53d573b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45263a6d-d817-4140-be69-9f18aca9505a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5982604d-49bd-4404-8442-e30dc2b478b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45263a6d-d817-4140-be69-9f18aca9505a",
                    "LayerId": "de57dea8-f21b-4c65-94ee-383a5c57f284"
                },
                {
                    "id": "469e2e27-8be3-4cb3-85b8-e8c31ba646e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45263a6d-d817-4140-be69-9f18aca9505a",
                    "LayerId": "4e808826-69d9-42b4-a328-9fa0cf83517a"
                }
            ]
        },
        {
            "id": "13fc8ceb-42c0-49e1-8fa8-d7504f4b7a40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb55f791-3a85-4d82-a85c-91dcce3a4849",
            "compositeImage": {
                "id": "293d5b8c-cfa5-4b23-849a-0af7ce025404",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13fc8ceb-42c0-49e1-8fa8-d7504f4b7a40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2173cf3b-dc15-47f7-b203-1f750e8bad66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13fc8ceb-42c0-49e1-8fa8-d7504f4b7a40",
                    "LayerId": "de57dea8-f21b-4c65-94ee-383a5c57f284"
                },
                {
                    "id": "615cee78-2e62-49a3-bc03-7f4a23395a6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13fc8ceb-42c0-49e1-8fa8-d7504f4b7a40",
                    "LayerId": "4e808826-69d9-42b4-a328-9fa0cf83517a"
                }
            ]
        },
        {
            "id": "a72b9694-6935-40e8-b43d-7cc8005bb77f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb55f791-3a85-4d82-a85c-91dcce3a4849",
            "compositeImage": {
                "id": "a8a46fea-8cbc-419a-9b73-650df15f4322",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a72b9694-6935-40e8-b43d-7cc8005bb77f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6ce6872-9758-45e4-bbc6-117b5dcc1abc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a72b9694-6935-40e8-b43d-7cc8005bb77f",
                    "LayerId": "de57dea8-f21b-4c65-94ee-383a5c57f284"
                },
                {
                    "id": "2f132e35-75fd-4a40-bbb1-1e7000ba9e42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a72b9694-6935-40e8-b43d-7cc8005bb77f",
                    "LayerId": "4e808826-69d9-42b4-a328-9fa0cf83517a"
                }
            ]
        },
        {
            "id": "37f2d9d8-2118-47cf-8756-9c2f5ac66024",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb55f791-3a85-4d82-a85c-91dcce3a4849",
            "compositeImage": {
                "id": "52e87a56-5351-4758-8f39-02d60f589dc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37f2d9d8-2118-47cf-8756-9c2f5ac66024",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90e457ff-5391-4401-bcd4-5834245c20f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37f2d9d8-2118-47cf-8756-9c2f5ac66024",
                    "LayerId": "de57dea8-f21b-4c65-94ee-383a5c57f284"
                },
                {
                    "id": "58bace3b-16e8-4c70-954a-e802f77bae12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37f2d9d8-2118-47cf-8756-9c2f5ac66024",
                    "LayerId": "4e808826-69d9-42b4-a328-9fa0cf83517a"
                }
            ]
        },
        {
            "id": "9ef00694-b498-4fb3-9dd7-38c41a0cbba5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb55f791-3a85-4d82-a85c-91dcce3a4849",
            "compositeImage": {
                "id": "acc10f7a-3cc6-4ea0-b3c5-551a449aa235",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ef00694-b498-4fb3-9dd7-38c41a0cbba5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "861ae32b-da7d-4915-83af-bf3abcc14312",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ef00694-b498-4fb3-9dd7-38c41a0cbba5",
                    "LayerId": "de57dea8-f21b-4c65-94ee-383a5c57f284"
                },
                {
                    "id": "5bbbd044-a897-4f1f-8001-68384c7153ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ef00694-b498-4fb3-9dd7-38c41a0cbba5",
                    "LayerId": "4e808826-69d9-42b4-a328-9fa0cf83517a"
                }
            ]
        },
        {
            "id": "06894e5a-75b2-4518-924b-bbeaf851e33e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb55f791-3a85-4d82-a85c-91dcce3a4849",
            "compositeImage": {
                "id": "a7d56bbd-bcd5-42b7-a4c7-3ef92259fdaf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06894e5a-75b2-4518-924b-bbeaf851e33e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70b4d8a3-4065-47ff-af5e-5ad44b804a5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06894e5a-75b2-4518-924b-bbeaf851e33e",
                    "LayerId": "de57dea8-f21b-4c65-94ee-383a5c57f284"
                },
                {
                    "id": "b48ddcae-2764-43ff-a121-392877c0f0c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06894e5a-75b2-4518-924b-bbeaf851e33e",
                    "LayerId": "4e808826-69d9-42b4-a328-9fa0cf83517a"
                }
            ]
        },
        {
            "id": "e4a5895b-7d55-42d6-a776-7ae352735fd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb55f791-3a85-4d82-a85c-91dcce3a4849",
            "compositeImage": {
                "id": "9197a9a2-8dd4-4821-a6d7-976734abd27c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4a5895b-7d55-42d6-a776-7ae352735fd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbdf3d6c-b1ac-4cf6-90d6-ea7615fe0dbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4a5895b-7d55-42d6-a776-7ae352735fd8",
                    "LayerId": "de57dea8-f21b-4c65-94ee-383a5c57f284"
                },
                {
                    "id": "228af893-0b4e-4f2d-a6b0-5329e4d866df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4a5895b-7d55-42d6-a776-7ae352735fd8",
                    "LayerId": "4e808826-69d9-42b4-a328-9fa0cf83517a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "de57dea8-f21b-4c65-94ee-383a5c57f284",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cb55f791-3a85-4d82-a85c-91dcce3a4849",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "4e808826-69d9-42b4-a328-9fa0cf83517a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cb55f791-3a85-4d82-a85c-91dcce3a4849",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 127
}