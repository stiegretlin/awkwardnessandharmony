{
    "id": "d87b2fdb-1133-47e3-834f-4518f38cc7de",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5bcb768b-18f9-4fb6-89f2-91e816b9830e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d87b2fdb-1133-47e3-834f-4518f38cc7de",
            "compositeImage": {
                "id": "79ee82bb-e010-407b-a37a-c9f0ab79833c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bcb768b-18f9-4fb6-89f2-91e816b9830e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd4f7813-73b8-4974-8a23-04377366eeb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bcb768b-18f9-4fb6-89f2-91e816b9830e",
                    "LayerId": "7887e04a-e1c9-4cae-a794-154bfaa9a5e5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7887e04a-e1c9-4cae-a794-154bfaa9a5e5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d87b2fdb-1133-47e3-834f-4518f38cc7de",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}