{
    "id": "a7b5df2e-1980-4dbc-93f1-5faf35bec8b4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_abby_body_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 1,
    "bbox_right": 62,
    "bbox_top": 55,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "40de05c7-d8e9-4f03-8605-282ed44bf133",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7b5df2e-1980-4dbc-93f1-5faf35bec8b4",
            "compositeImage": {
                "id": "b7f8efd3-0a65-4e2a-b5f5-fb9417786918",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40de05c7-d8e9-4f03-8605-282ed44bf133",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a89fcb0-6836-4049-a5e5-4b5e4e101ab7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40de05c7-d8e9-4f03-8605-282ed44bf133",
                    "LayerId": "7581ab0d-4c8d-4144-8b4f-493f18bf88a7"
                }
            ]
        },
        {
            "id": "c7333b70-6319-4948-a285-1cfbd01fd872",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7b5df2e-1980-4dbc-93f1-5faf35bec8b4",
            "compositeImage": {
                "id": "d929fccb-e260-41a5-8309-1608a71d55a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7333b70-6319-4948-a285-1cfbd01fd872",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e960d95-b05e-4341-b1d1-4e902555e39a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7333b70-6319-4948-a285-1cfbd01fd872",
                    "LayerId": "7581ab0d-4c8d-4144-8b4f-493f18bf88a7"
                }
            ]
        },
        {
            "id": "b3ff0706-ea35-423b-b1d1-d555dd81ee01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7b5df2e-1980-4dbc-93f1-5faf35bec8b4",
            "compositeImage": {
                "id": "27a0b28d-9f43-4e39-adfe-bd5de3c22bb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3ff0706-ea35-423b-b1d1-d555dd81ee01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca0237f1-bf0f-48f7-ad14-18859161fc8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3ff0706-ea35-423b-b1d1-d555dd81ee01",
                    "LayerId": "7581ab0d-4c8d-4144-8b4f-493f18bf88a7"
                }
            ]
        },
        {
            "id": "bb1011af-eb68-422b-a81f-a669fa160835",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7b5df2e-1980-4dbc-93f1-5faf35bec8b4",
            "compositeImage": {
                "id": "75691f7b-e7e1-497d-ade9-11e61194e134",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb1011af-eb68-422b-a81f-a669fa160835",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97260228-898b-4867-b559-02657f1a6d31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb1011af-eb68-422b-a81f-a669fa160835",
                    "LayerId": "7581ab0d-4c8d-4144-8b4f-493f18bf88a7"
                }
            ]
        },
        {
            "id": "23e1ee13-904e-4ef8-9d8f-cae2f92761de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7b5df2e-1980-4dbc-93f1-5faf35bec8b4",
            "compositeImage": {
                "id": "ed14dc51-c677-41b3-8429-f47890775257",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23e1ee13-904e-4ef8-9d8f-cae2f92761de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5eb37fbc-31dc-4fe0-aa84-bd89d6ef085f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23e1ee13-904e-4ef8-9d8f-cae2f92761de",
                    "LayerId": "7581ab0d-4c8d-4144-8b4f-493f18bf88a7"
                }
            ]
        },
        {
            "id": "ef8de6ef-1efb-4a67-b58a-97e8e598d1c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7b5df2e-1980-4dbc-93f1-5faf35bec8b4",
            "compositeImage": {
                "id": "c2d35c1f-a7a7-4467-a961-907feabb9a29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef8de6ef-1efb-4a67-b58a-97e8e598d1c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09dbae37-6f8d-41fa-ba57-8a8a2d2edb61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef8de6ef-1efb-4a67-b58a-97e8e598d1c6",
                    "LayerId": "7581ab0d-4c8d-4144-8b4f-493f18bf88a7"
                }
            ]
        },
        {
            "id": "862dc559-dc68-4a50-ae25-d1900e66833e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7b5df2e-1980-4dbc-93f1-5faf35bec8b4",
            "compositeImage": {
                "id": "f0a62f3b-7858-4076-8b7d-027cbf217960",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "862dc559-dc68-4a50-ae25-d1900e66833e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "395c7d89-6505-4a54-84e7-07778cf773f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "862dc559-dc68-4a50-ae25-d1900e66833e",
                    "LayerId": "7581ab0d-4c8d-4144-8b4f-493f18bf88a7"
                }
            ]
        },
        {
            "id": "00b67f96-e830-4423-93f8-0141c1d7d2cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7b5df2e-1980-4dbc-93f1-5faf35bec8b4",
            "compositeImage": {
                "id": "10aebaa8-83dc-4680-ae8d-0ec6bebcc24f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00b67f96-e830-4423-93f8-0141c1d7d2cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6472e5ad-90be-43b8-af72-c60588e42e2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00b67f96-e830-4423-93f8-0141c1d7d2cb",
                    "LayerId": "7581ab0d-4c8d-4144-8b4f-493f18bf88a7"
                }
            ]
        },
        {
            "id": "6fd35092-6ae9-4860-bfbf-154ffd0eac69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7b5df2e-1980-4dbc-93f1-5faf35bec8b4",
            "compositeImage": {
                "id": "e7e16e13-ea46-4899-af4c-abf1e81d2ff0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fd35092-6ae9-4860-bfbf-154ffd0eac69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c069766-95d0-483a-9eee-e2f3b788887d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fd35092-6ae9-4860-bfbf-154ffd0eac69",
                    "LayerId": "7581ab0d-4c8d-4144-8b4f-493f18bf88a7"
                }
            ]
        },
        {
            "id": "d4331ac8-0ec9-4fde-8248-f51224501ed2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7b5df2e-1980-4dbc-93f1-5faf35bec8b4",
            "compositeImage": {
                "id": "2bc2c792-629e-44b7-8ee9-8cccf15889ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4331ac8-0ec9-4fde-8248-f51224501ed2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51c6a21f-672e-45c4-aefd-9faf31f3ebfd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4331ac8-0ec9-4fde-8248-f51224501ed2",
                    "LayerId": "7581ab0d-4c8d-4144-8b4f-493f18bf88a7"
                }
            ]
        },
        {
            "id": "304703bb-399b-4e20-a853-3bbd1490b4da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7b5df2e-1980-4dbc-93f1-5faf35bec8b4",
            "compositeImage": {
                "id": "046c9dc9-97ba-455c-b0b7-8daa0ce9b7da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "304703bb-399b-4e20-a853-3bbd1490b4da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64c08210-b7ae-45a5-8ea4-5500ec75b9dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "304703bb-399b-4e20-a853-3bbd1490b4da",
                    "LayerId": "7581ab0d-4c8d-4144-8b4f-493f18bf88a7"
                }
            ]
        },
        {
            "id": "51f35523-b1c6-4dd2-b555-fa80a815e5e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7b5df2e-1980-4dbc-93f1-5faf35bec8b4",
            "compositeImage": {
                "id": "27287555-427e-4903-9f6c-e1432693b55f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51f35523-b1c6-4dd2-b555-fa80a815e5e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a56516a-635b-4b3d-bd91-4f3833be6268",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51f35523-b1c6-4dd2-b555-fa80a815e5e2",
                    "LayerId": "7581ab0d-4c8d-4144-8b4f-493f18bf88a7"
                }
            ]
        },
        {
            "id": "a6083d92-8d68-406f-a1b9-0dbb9fd9ca0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7b5df2e-1980-4dbc-93f1-5faf35bec8b4",
            "compositeImage": {
                "id": "237d9a50-684d-4a70-b380-1bac0e8a1687",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6083d92-8d68-406f-a1b9-0dbb9fd9ca0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a350dc61-c7a5-4bae-aee9-6ea19b09e45a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6083d92-8d68-406f-a1b9-0dbb9fd9ca0c",
                    "LayerId": "7581ab0d-4c8d-4144-8b4f-493f18bf88a7"
                }
            ]
        },
        {
            "id": "b50edb58-1f31-4a68-bbca-66eed117e732",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7b5df2e-1980-4dbc-93f1-5faf35bec8b4",
            "compositeImage": {
                "id": "647cd267-136c-4d6f-ae6d-e01ec74b2aba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b50edb58-1f31-4a68-bbca-66eed117e732",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "796165a0-ab7c-4659-9987-63902c917d32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b50edb58-1f31-4a68-bbca-66eed117e732",
                    "LayerId": "7581ab0d-4c8d-4144-8b4f-493f18bf88a7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "7581ab0d-4c8d-4144-8b4f-493f18bf88a7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a7b5df2e-1980-4dbc-93f1-5faf35bec8b4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 127
}