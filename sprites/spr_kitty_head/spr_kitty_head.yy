{
    "id": "23ebfca6-7308-4d6e-b607-b7a7c7407479",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_kitty_head",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 16,
    "bbox_right": 54,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "de3f02be-dbff-48ef-95d8-2f5250f53269",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23ebfca6-7308-4d6e-b607-b7a7c7407479",
            "compositeImage": {
                "id": "6bb1e45a-efc5-48df-bcae-4f2cbaa555a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de3f02be-dbff-48ef-95d8-2f5250f53269",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc0b86a0-f8dd-4f84-b80c-b50d2f2a1a26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de3f02be-dbff-48ef-95d8-2f5250f53269",
                    "LayerId": "1cc7d64d-4cce-4433-9c47-dff55eecba19"
                },
                {
                    "id": "bfc91bff-5382-408f-83da-b303c2ce39fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de3f02be-dbff-48ef-95d8-2f5250f53269",
                    "LayerId": "945c4d14-74cd-4a0f-8601-b82d473b3442"
                }
            ]
        },
        {
            "id": "34be7e03-f4be-4c76-a0c6-496d4f7f9513",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23ebfca6-7308-4d6e-b607-b7a7c7407479",
            "compositeImage": {
                "id": "048ae3f3-030e-4890-b52f-a94ea93f6c37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34be7e03-f4be-4c76-a0c6-496d4f7f9513",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b239376d-dd3e-4aa7-befa-cf8f36e07085",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34be7e03-f4be-4c76-a0c6-496d4f7f9513",
                    "LayerId": "1cc7d64d-4cce-4433-9c47-dff55eecba19"
                },
                {
                    "id": "1f0380d5-f938-441a-bc4b-bd6fc70955c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34be7e03-f4be-4c76-a0c6-496d4f7f9513",
                    "LayerId": "945c4d14-74cd-4a0f-8601-b82d473b3442"
                }
            ]
        },
        {
            "id": "3a7e1914-b85b-4d8c-823f-410f606d40a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23ebfca6-7308-4d6e-b607-b7a7c7407479",
            "compositeImage": {
                "id": "40363ad2-ada0-44cf-8f6b-7d201058812b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a7e1914-b85b-4d8c-823f-410f606d40a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8fb5bcf-34e0-4d2d-8202-55f4615bde73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a7e1914-b85b-4d8c-823f-410f606d40a4",
                    "LayerId": "1cc7d64d-4cce-4433-9c47-dff55eecba19"
                },
                {
                    "id": "b4fdf1b0-4826-4acb-be5e-bf21b390db98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a7e1914-b85b-4d8c-823f-410f606d40a4",
                    "LayerId": "945c4d14-74cd-4a0f-8601-b82d473b3442"
                }
            ]
        },
        {
            "id": "4e13a4e0-34b5-4e9b-82ea-06a2045d9a92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23ebfca6-7308-4d6e-b607-b7a7c7407479",
            "compositeImage": {
                "id": "e666c993-273b-4b44-866b-fae56f525a1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e13a4e0-34b5-4e9b-82ea-06a2045d9a92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7496ada0-1677-4a65-9aa1-69bff19c661d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e13a4e0-34b5-4e9b-82ea-06a2045d9a92",
                    "LayerId": "1cc7d64d-4cce-4433-9c47-dff55eecba19"
                },
                {
                    "id": "74db665f-e322-48ec-91c9-3602e809bbdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e13a4e0-34b5-4e9b-82ea-06a2045d9a92",
                    "LayerId": "945c4d14-74cd-4a0f-8601-b82d473b3442"
                }
            ]
        },
        {
            "id": "6df38408-5e07-4f62-b1c7-317e16f86e65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23ebfca6-7308-4d6e-b607-b7a7c7407479",
            "compositeImage": {
                "id": "844f7272-ac76-4ec6-be99-9d0f62cca685",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6df38408-5e07-4f62-b1c7-317e16f86e65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d53fb6eb-8a34-496b-8fd7-615bea54c60d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6df38408-5e07-4f62-b1c7-317e16f86e65",
                    "LayerId": "1cc7d64d-4cce-4433-9c47-dff55eecba19"
                },
                {
                    "id": "16368dee-2336-4565-a12b-127bb9001a22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6df38408-5e07-4f62-b1c7-317e16f86e65",
                    "LayerId": "945c4d14-74cd-4a0f-8601-b82d473b3442"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 70,
    "layers": [
        {
            "id": "945c4d14-74cd-4a0f-8601-b82d473b3442",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "23ebfca6-7308-4d6e-b607-b7a7c7407479",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "1cc7d64d-4cce-4433-9c47-dff55eecba19",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "23ebfca6-7308-4d6e-b607-b7a7c7407479",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 9,
            "visible": false
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 33,
    "yorig": 110
}