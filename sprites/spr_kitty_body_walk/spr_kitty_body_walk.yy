{
    "id": "77c99071-8863-4e38-b584-f2545ad0420d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_kitty_body_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 75,
    "bbox_top": 63,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0aad93c3-30ad-4c5d-a13c-f4a1f5dfb132",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77c99071-8863-4e38-b584-f2545ad0420d",
            "compositeImage": {
                "id": "cd8dc09e-4384-401b-afb7-2dccc0e0e6b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0aad93c3-30ad-4c5d-a13c-f4a1f5dfb132",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d40ce20-2a6c-4c75-9a30-b45619219229",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0aad93c3-30ad-4c5d-a13c-f4a1f5dfb132",
                    "LayerId": "833415ae-cbfd-49ca-9c51-d928b2987bf7"
                }
            ]
        },
        {
            "id": "dfb31bd7-4eec-4953-a424-59fc7e66163e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77c99071-8863-4e38-b584-f2545ad0420d",
            "compositeImage": {
                "id": "8a4454b1-749d-4705-a56e-08e41d850ce2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfb31bd7-4eec-4953-a424-59fc7e66163e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aac8518a-a077-4b75-af2d-a8bb57f8c432",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfb31bd7-4eec-4953-a424-59fc7e66163e",
                    "LayerId": "833415ae-cbfd-49ca-9c51-d928b2987bf7"
                }
            ]
        },
        {
            "id": "c315f1e5-5f56-4459-94bd-a42989faf497",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77c99071-8863-4e38-b584-f2545ad0420d",
            "compositeImage": {
                "id": "8635167c-941a-42bd-aa6c-f67bf6b811a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c315f1e5-5f56-4459-94bd-a42989faf497",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "567c9d6a-b984-4b0e-93d5-a055ff5e7f2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c315f1e5-5f56-4459-94bd-a42989faf497",
                    "LayerId": "833415ae-cbfd-49ca-9c51-d928b2987bf7"
                }
            ]
        },
        {
            "id": "c87a07f8-111b-422e-a56b-1c3abdc0fcfc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77c99071-8863-4e38-b584-f2545ad0420d",
            "compositeImage": {
                "id": "45db7445-d16f-4443-93d9-fb6f67ac5693",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c87a07f8-111b-422e-a56b-1c3abdc0fcfc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebc73342-90e2-4906-8ee6-33ac75f2f718",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c87a07f8-111b-422e-a56b-1c3abdc0fcfc",
                    "LayerId": "833415ae-cbfd-49ca-9c51-d928b2987bf7"
                }
            ]
        },
        {
            "id": "5045fb43-2aae-4ff5-b5d3-9b38e6d68f74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77c99071-8863-4e38-b584-f2545ad0420d",
            "compositeImage": {
                "id": "3883fe36-0b38-48fd-92e4-5095df29c035",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5045fb43-2aae-4ff5-b5d3-9b38e6d68f74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27d63761-f3e9-4762-846a-389202f377b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5045fb43-2aae-4ff5-b5d3-9b38e6d68f74",
                    "LayerId": "833415ae-cbfd-49ca-9c51-d928b2987bf7"
                }
            ]
        },
        {
            "id": "7f294c48-4ed7-4f2d-8d03-1473dfb1b70b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77c99071-8863-4e38-b584-f2545ad0420d",
            "compositeImage": {
                "id": "795caac3-998a-425d-888d-b0c724a3745f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f294c48-4ed7-4f2d-8d03-1473dfb1b70b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4163709d-ce1d-4d7a-8294-584dfe2c2082",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f294c48-4ed7-4f2d-8d03-1473dfb1b70b",
                    "LayerId": "833415ae-cbfd-49ca-9c51-d928b2987bf7"
                }
            ]
        },
        {
            "id": "a60028f9-3c44-4f1e-9f16-d9d8bc040ed4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77c99071-8863-4e38-b584-f2545ad0420d",
            "compositeImage": {
                "id": "2575921a-c67a-4127-9dbb-e928a86384af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a60028f9-3c44-4f1e-9f16-d9d8bc040ed4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "348e80f8-e0b9-49a5-ba38-d8badfdd99ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a60028f9-3c44-4f1e-9f16-d9d8bc040ed4",
                    "LayerId": "833415ae-cbfd-49ca-9c51-d928b2987bf7"
                }
            ]
        },
        {
            "id": "add0b53a-ca95-433b-b18c-1434646cfd96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77c99071-8863-4e38-b584-f2545ad0420d",
            "compositeImage": {
                "id": "1e39dd95-07a6-40fa-8432-8d8616c4b98a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "add0b53a-ca95-433b-b18c-1434646cfd96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "658d0283-6618-4dc1-95f1-8ad10d812387",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "add0b53a-ca95-433b-b18c-1434646cfd96",
                    "LayerId": "833415ae-cbfd-49ca-9c51-d928b2987bf7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "833415ae-cbfd-49ca-9c51-d928b2987bf7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77c99071-8863-4e38-b584-f2545ad0420d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 33,
    "yorig": 127
}