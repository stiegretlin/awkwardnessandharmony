{
    "id": "3b2e0152-500f-4ce5-aa98-0d5e1229a46e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wall_abby",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "51481e98-f387-4d2d-b2aa-e42faa10f083",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b2e0152-500f-4ce5-aa98-0d5e1229a46e",
            "compositeImage": {
                "id": "fe0b5281-06d6-4ed5-be75-7900c45fcfde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51481e98-f387-4d2d-b2aa-e42faa10f083",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8678d308-8001-4106-8cdd-cfd02ee583bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51481e98-f387-4d2d-b2aa-e42faa10f083",
                    "LayerId": "36225b1f-4f9f-4df6-b684-d7c6cf0bba0e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "36225b1f-4f9f-4df6-b684-d7c6cf0bba0e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3b2e0152-500f-4ce5-aa98-0d5e1229a46e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}