{
    "id": "2d1b4923-736e-4261-bbbc-24e62e42db2a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bark02",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 315,
    "bbox_left": -39,
    "bbox_right": 143,
    "bbox_top": 91,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fa08088a-6e94-497e-ad2b-21b48a924259",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d1b4923-736e-4261-bbbc-24e62e42db2a",
            "compositeImage": {
                "id": "30e1eae9-7fcd-45de-b4c7-4ee4f781a1e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa08088a-6e94-497e-ad2b-21b48a924259",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9920340-762b-45a0-94df-cc032f6c7cae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa08088a-6e94-497e-ad2b-21b48a924259",
                    "LayerId": "be3cab78-e38d-4f36-8803-c2bb70791d0b"
                },
                {
                    "id": "13dbecc5-bf4f-4f3a-ba50-54ea48c26610",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa08088a-6e94-497e-ad2b-21b48a924259",
                    "LayerId": "1057c9c3-a521-4a46-972b-348b41ec1866"
                }
            ]
        },
        {
            "id": "e3424580-4aa2-4224-9502-1267f7e22059",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d1b4923-736e-4261-bbbc-24e62e42db2a",
            "compositeImage": {
                "id": "b9852cc0-a04a-47e3-9cac-5ee8895e7ba3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3424580-4aa2-4224-9502-1267f7e22059",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34fe60ae-052d-45fb-b293-4ae7c4808b82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3424580-4aa2-4224-9502-1267f7e22059",
                    "LayerId": "be3cab78-e38d-4f36-8803-c2bb70791d0b"
                },
                {
                    "id": "8a54d6cf-d167-4876-a24b-1180a379dc8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3424580-4aa2-4224-9502-1267f7e22059",
                    "LayerId": "1057c9c3-a521-4a46-972b-348b41ec1866"
                }
            ]
        },
        {
            "id": "888f3d0b-f96b-41b8-95a4-c6357dcaebe4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d1b4923-736e-4261-bbbc-24e62e42db2a",
            "compositeImage": {
                "id": "dd476f66-6353-48c0-a7bb-e04b2540f787",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "888f3d0b-f96b-41b8-95a4-c6357dcaebe4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "092097a2-062f-480f-97a5-55dbc03d4993",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "888f3d0b-f96b-41b8-95a4-c6357dcaebe4",
                    "LayerId": "be3cab78-e38d-4f36-8803-c2bb70791d0b"
                },
                {
                    "id": "2ca96c4b-d2f2-4421-bcfb-99da44fe0b66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "888f3d0b-f96b-41b8-95a4-c6357dcaebe4",
                    "LayerId": "1057c9c3-a521-4a46-972b-348b41ec1866"
                }
            ]
        },
        {
            "id": "6abeec58-7a77-4238-9da3-8f9175ac1f29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d1b4923-736e-4261-bbbc-24e62e42db2a",
            "compositeImage": {
                "id": "96ec8670-953a-4b33-8418-a29b73e32778",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6abeec58-7a77-4238-9da3-8f9175ac1f29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bbaad55-ff7d-4459-93b6-2a2b36ca2564",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6abeec58-7a77-4238-9da3-8f9175ac1f29",
                    "LayerId": "be3cab78-e38d-4f36-8803-c2bb70791d0b"
                },
                {
                    "id": "8acf15e1-a861-4209-8431-24872b3844ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6abeec58-7a77-4238-9da3-8f9175ac1f29",
                    "LayerId": "1057c9c3-a521-4a46-972b-348b41ec1866"
                }
            ]
        },
        {
            "id": "ba4d5cfc-e9bf-4cfb-94e7-d041a3e71a25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d1b4923-736e-4261-bbbc-24e62e42db2a",
            "compositeImage": {
                "id": "62681048-e32d-4b2e-9319-6aa5b30486a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba4d5cfc-e9bf-4cfb-94e7-d041a3e71a25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c503dab-f43d-48c9-a761-93d118b7d446",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba4d5cfc-e9bf-4cfb-94e7-d041a3e71a25",
                    "LayerId": "be3cab78-e38d-4f36-8803-c2bb70791d0b"
                },
                {
                    "id": "b6e4199f-88e8-4e7a-af30-efe0ca6a0499",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba4d5cfc-e9bf-4cfb-94e7-d041a3e71a25",
                    "LayerId": "1057c9c3-a521-4a46-972b-348b41ec1866"
                }
            ]
        },
        {
            "id": "e16117f4-034d-48d6-b7fb-04cfea6d5490",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d1b4923-736e-4261-bbbc-24e62e42db2a",
            "compositeImage": {
                "id": "70b0001c-242b-451f-ab26-547ea7282dd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e16117f4-034d-48d6-b7fb-04cfea6d5490",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fdcfaf1-a21c-42ee-b332-9b48b8629373",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e16117f4-034d-48d6-b7fb-04cfea6d5490",
                    "LayerId": "be3cab78-e38d-4f36-8803-c2bb70791d0b"
                },
                {
                    "id": "9909334f-a602-4ad4-9cf5-c7bcf36656c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e16117f4-034d-48d6-b7fb-04cfea6d5490",
                    "LayerId": "1057c9c3-a521-4a46-972b-348b41ec1866"
                }
            ]
        },
        {
            "id": "bb542ecc-7306-412b-87f8-928165054b8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d1b4923-736e-4261-bbbc-24e62e42db2a",
            "compositeImage": {
                "id": "71993206-8701-4197-985b-17fc7904799c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb542ecc-7306-412b-87f8-928165054b8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "deea3234-e6a2-4a97-b70f-c5882c614011",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb542ecc-7306-412b-87f8-928165054b8b",
                    "LayerId": "be3cab78-e38d-4f36-8803-c2bb70791d0b"
                },
                {
                    "id": "8f50efaa-0da4-403f-a699-64e9937d744d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb542ecc-7306-412b-87f8-928165054b8b",
                    "LayerId": "1057c9c3-a521-4a46-972b-348b41ec1866"
                }
            ]
        },
        {
            "id": "df472fcc-76a8-4838-8d84-81b7689c6285",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d1b4923-736e-4261-bbbc-24e62e42db2a",
            "compositeImage": {
                "id": "2adf81c7-0b53-459f-8d24-2125694fe619",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df472fcc-76a8-4838-8d84-81b7689c6285",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f22f5790-7f71-4ce0-9a85-9106ea8a4492",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df472fcc-76a8-4838-8d84-81b7689c6285",
                    "LayerId": "be3cab78-e38d-4f36-8803-c2bb70791d0b"
                },
                {
                    "id": "a4adf1d2-f0f6-411e-87dd-850dae2c0329",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df472fcc-76a8-4838-8d84-81b7689c6285",
                    "LayerId": "1057c9c3-a521-4a46-972b-348b41ec1866"
                }
            ]
        },
        {
            "id": "ab79c3bd-618f-43bd-890b-843565790642",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d1b4923-736e-4261-bbbc-24e62e42db2a",
            "compositeImage": {
                "id": "594cea3a-47be-4ebb-a93a-857b6713c2bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab79c3bd-618f-43bd-890b-843565790642",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "507a116b-15fe-4108-bf2c-c6be631abdb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab79c3bd-618f-43bd-890b-843565790642",
                    "LayerId": "be3cab78-e38d-4f36-8803-c2bb70791d0b"
                },
                {
                    "id": "44e6b837-d646-437d-b5d2-c15803f50104",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab79c3bd-618f-43bd-890b-843565790642",
                    "LayerId": "1057c9c3-a521-4a46-972b-348b41ec1866"
                }
            ]
        },
        {
            "id": "3ea17cab-763d-41dc-8a5e-0e359b2a7a5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d1b4923-736e-4261-bbbc-24e62e42db2a",
            "compositeImage": {
                "id": "cb62025b-7809-44db-9bb9-3bc0d2e261cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ea17cab-763d-41dc-8a5e-0e359b2a7a5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d44c3ca-d3b4-435f-bc84-9754a99b9390",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ea17cab-763d-41dc-8a5e-0e359b2a7a5f",
                    "LayerId": "be3cab78-e38d-4f36-8803-c2bb70791d0b"
                },
                {
                    "id": "6634b85b-0c60-4e45-9c41-2bc5a7ac2d3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ea17cab-763d-41dc-8a5e-0e359b2a7a5f",
                    "LayerId": "1057c9c3-a521-4a46-972b-348b41ec1866"
                }
            ]
        },
        {
            "id": "b78439e1-c42b-4835-a7e1-f03faff13678",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d1b4923-736e-4261-bbbc-24e62e42db2a",
            "compositeImage": {
                "id": "99114ee9-20c2-4456-9017-9305b26d2696",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b78439e1-c42b-4835-a7e1-f03faff13678",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a59f06eb-fd63-4f33-acc9-563e1ffb1944",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b78439e1-c42b-4835-a7e1-f03faff13678",
                    "LayerId": "be3cab78-e38d-4f36-8803-c2bb70791d0b"
                },
                {
                    "id": "a750e4c3-9214-4d35-a3c8-151b7ed9fa5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b78439e1-c42b-4835-a7e1-f03faff13678",
                    "LayerId": "1057c9c3-a521-4a46-972b-348b41ec1866"
                }
            ]
        },
        {
            "id": "db94bfde-3b79-4fa1-a829-6559552d2c3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d1b4923-736e-4261-bbbc-24e62e42db2a",
            "compositeImage": {
                "id": "eb8962b1-8792-4e1f-a736-266b4ae8e104",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db94bfde-3b79-4fa1-a829-6559552d2c3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea7bb22c-5611-4b97-8b10-dc4a6c384c86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db94bfde-3b79-4fa1-a829-6559552d2c3a",
                    "LayerId": "be3cab78-e38d-4f36-8803-c2bb70791d0b"
                },
                {
                    "id": "1a2514e6-2131-495a-870f-f8e544a082de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db94bfde-3b79-4fa1-a829-6559552d2c3a",
                    "LayerId": "1057c9c3-a521-4a46-972b-348b41ec1866"
                }
            ]
        },
        {
            "id": "2f447a27-1ddc-4080-b724-3c95d2242f06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d1b4923-736e-4261-bbbc-24e62e42db2a",
            "compositeImage": {
                "id": "62ece9a6-79f4-458b-9adb-85d9ed00dd0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f447a27-1ddc-4080-b724-3c95d2242f06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d94c278-772f-46c8-8ae2-c635c5db33e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f447a27-1ddc-4080-b724-3c95d2242f06",
                    "LayerId": "be3cab78-e38d-4f36-8803-c2bb70791d0b"
                },
                {
                    "id": "4fc44c3f-9dea-49ad-bc0c-940c37fe9097",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f447a27-1ddc-4080-b724-3c95d2242f06",
                    "LayerId": "1057c9c3-a521-4a46-972b-348b41ec1866"
                }
            ]
        },
        {
            "id": "a6c0f6fa-edf4-44fd-af9e-1b8cb01110be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d1b4923-736e-4261-bbbc-24e62e42db2a",
            "compositeImage": {
                "id": "46e30032-1a90-42bc-a573-cfbf89b69374",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6c0f6fa-edf4-44fd-af9e-1b8cb01110be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4a352f2-05cc-409f-ae05-23403aa61bbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6c0f6fa-edf4-44fd-af9e-1b8cb01110be",
                    "LayerId": "be3cab78-e38d-4f36-8803-c2bb70791d0b"
                },
                {
                    "id": "6544a2de-63a6-429c-a99e-4ae379ed0311",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6c0f6fa-edf4-44fd-af9e-1b8cb01110be",
                    "LayerId": "1057c9c3-a521-4a46-972b-348b41ec1866"
                }
            ]
        },
        {
            "id": "97802baa-a63e-426f-867d-bd935241c438",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d1b4923-736e-4261-bbbc-24e62e42db2a",
            "compositeImage": {
                "id": "549ff66c-3b31-4e3d-825b-bbae35eb35ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97802baa-a63e-426f-867d-bd935241c438",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93b51ee7-9c28-43af-9fbb-76ac64a65979",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97802baa-a63e-426f-867d-bd935241c438",
                    "LayerId": "be3cab78-e38d-4f36-8803-c2bb70791d0b"
                },
                {
                    "id": "2381bb74-ea67-4420-8342-94ac2ec1ff58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97802baa-a63e-426f-867d-bd935241c438",
                    "LayerId": "1057c9c3-a521-4a46-972b-348b41ec1866"
                }
            ]
        },
        {
            "id": "002d32b1-9526-4ffa-a87f-81939e8d80a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d1b4923-736e-4261-bbbc-24e62e42db2a",
            "compositeImage": {
                "id": "381aa803-f3a3-47c5-bd4f-661711a9bed3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "002d32b1-9526-4ffa-a87f-81939e8d80a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e464528-f031-4bbc-9fa5-3856eef858b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "002d32b1-9526-4ffa-a87f-81939e8d80a5",
                    "LayerId": "be3cab78-e38d-4f36-8803-c2bb70791d0b"
                },
                {
                    "id": "8e9bfd43-2568-4f0a-b94a-a2270454f697",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "002d32b1-9526-4ffa-a87f-81939e8d80a5",
                    "LayerId": "1057c9c3-a521-4a46-972b-348b41ec1866"
                }
            ]
        },
        {
            "id": "1e4b2a8d-dfd1-4efa-9281-4447e521f164",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d1b4923-736e-4261-bbbc-24e62e42db2a",
            "compositeImage": {
                "id": "366acf0e-2595-4c00-a02a-c0f17f1bace2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e4b2a8d-dfd1-4efa-9281-4447e521f164",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dca0d019-090a-45c8-ae34-b5ee77bb22c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e4b2a8d-dfd1-4efa-9281-4447e521f164",
                    "LayerId": "be3cab78-e38d-4f36-8803-c2bb70791d0b"
                },
                {
                    "id": "43ec1d6d-771f-420f-829d-1f95fdbe8fb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e4b2a8d-dfd1-4efa-9281-4447e521f164",
                    "LayerId": "1057c9c3-a521-4a46-972b-348b41ec1866"
                }
            ]
        },
        {
            "id": "8e341709-ff56-4b80-9ab7-62522d58aed9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d1b4923-736e-4261-bbbc-24e62e42db2a",
            "compositeImage": {
                "id": "a7cd6a65-7b0f-4db9-bd14-5b71ea6fa705",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e341709-ff56-4b80-9ab7-62522d58aed9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8272f85b-e159-4f9f-ae49-c48eb64775d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e341709-ff56-4b80-9ab7-62522d58aed9",
                    "LayerId": "be3cab78-e38d-4f36-8803-c2bb70791d0b"
                },
                {
                    "id": "078b51d7-a1ae-4d96-a1cf-047670ed6dae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e341709-ff56-4b80-9ab7-62522d58aed9",
                    "LayerId": "1057c9c3-a521-4a46-972b-348b41ec1866"
                }
            ]
        },
        {
            "id": "9483df00-81d1-423d-9071-9e96b6110f3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d1b4923-736e-4261-bbbc-24e62e42db2a",
            "compositeImage": {
                "id": "78a15d7a-dfc4-4011-8ec5-6a0fe7a79210",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9483df00-81d1-423d-9071-9e96b6110f3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e077eff9-bac1-4313-92d3-68906faf1eb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9483df00-81d1-423d-9071-9e96b6110f3e",
                    "LayerId": "be3cab78-e38d-4f36-8803-c2bb70791d0b"
                },
                {
                    "id": "5a7349b2-459e-4106-8f4b-fe51f2b6b46d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9483df00-81d1-423d-9071-9e96b6110f3e",
                    "LayerId": "1057c9c3-a521-4a46-972b-348b41ec1866"
                }
            ]
        },
        {
            "id": "fe5a2b52-e986-4bfa-83fa-bdc850d16923",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d1b4923-736e-4261-bbbc-24e62e42db2a",
            "compositeImage": {
                "id": "133d9962-7a70-40d5-889b-bffc6fc89541",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe5a2b52-e986-4bfa-83fa-bdc850d16923",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74f23aa3-037b-4279-85b2-9669cbcd324c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe5a2b52-e986-4bfa-83fa-bdc850d16923",
                    "LayerId": "be3cab78-e38d-4f36-8803-c2bb70791d0b"
                },
                {
                    "id": "4f7562f8-e993-490e-80eb-7b6c4692f255",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe5a2b52-e986-4bfa-83fa-bdc850d16923",
                    "LayerId": "1057c9c3-a521-4a46-972b-348b41ec1866"
                }
            ]
        },
        {
            "id": "099656ad-59c2-40e4-87e9-44225c49ca22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d1b4923-736e-4261-bbbc-24e62e42db2a",
            "compositeImage": {
                "id": "5371eecb-6501-455c-a150-58e5f4fb78f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "099656ad-59c2-40e4-87e9-44225c49ca22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4af69e9d-e01f-4ff9-a5ea-1cbaa81141d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "099656ad-59c2-40e4-87e9-44225c49ca22",
                    "LayerId": "be3cab78-e38d-4f36-8803-c2bb70791d0b"
                },
                {
                    "id": "d349d1f7-b83b-46aa-b0f2-9a1bb72f76d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "099656ad-59c2-40e4-87e9-44225c49ca22",
                    "LayerId": "1057c9c3-a521-4a46-972b-348b41ec1866"
                }
            ]
        },
        {
            "id": "1a49d67f-891e-4cb6-a884-ac6ba4652478",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d1b4923-736e-4261-bbbc-24e62e42db2a",
            "compositeImage": {
                "id": "26b43ee8-90f6-4a55-8e94-3b0d32d1d701",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a49d67f-891e-4cb6-a884-ac6ba4652478",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b85d72b-45f7-4a5b-814c-c289cbd6dd02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a49d67f-891e-4cb6-a884-ac6ba4652478",
                    "LayerId": "be3cab78-e38d-4f36-8803-c2bb70791d0b"
                },
                {
                    "id": "dcc43e48-91aa-4888-83d6-cf7465661f79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a49d67f-891e-4cb6-a884-ac6ba4652478",
                    "LayerId": "1057c9c3-a521-4a46-972b-348b41ec1866"
                }
            ]
        },
        {
            "id": "4079eff2-1c21-44bb-8420-fd88a57fddf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d1b4923-736e-4261-bbbc-24e62e42db2a",
            "compositeImage": {
                "id": "7e1a202d-363b-497c-8cd4-12b9a10c8067",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4079eff2-1c21-44bb-8420-fd88a57fddf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9af03642-6ffe-40aa-81ea-431d7e8856d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4079eff2-1c21-44bb-8420-fd88a57fddf2",
                    "LayerId": "be3cab78-e38d-4f36-8803-c2bb70791d0b"
                },
                {
                    "id": "003da0c8-c1a5-4a1f-b23d-c652f74be68e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4079eff2-1c21-44bb-8420-fd88a57fddf2",
                    "LayerId": "1057c9c3-a521-4a46-972b-348b41ec1866"
                }
            ]
        },
        {
            "id": "49894749-a401-4f47-8d42-50a5b1740d02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d1b4923-736e-4261-bbbc-24e62e42db2a",
            "compositeImage": {
                "id": "d6edeede-35f6-4f92-8d01-6b67dde65154",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49894749-a401-4f47-8d42-50a5b1740d02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48637304-dbee-4537-a647-32623e1bd225",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49894749-a401-4f47-8d42-50a5b1740d02",
                    "LayerId": "be3cab78-e38d-4f36-8803-c2bb70791d0b"
                },
                {
                    "id": "7e9b3f19-f8b0-47d2-a858-1a4f8f7ce5b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49894749-a401-4f47-8d42-50a5b1740d02",
                    "LayerId": "1057c9c3-a521-4a46-972b-348b41ec1866"
                }
            ]
        },
        {
            "id": "d460cb8c-d9ea-497c-833e-b2c479138e5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d1b4923-736e-4261-bbbc-24e62e42db2a",
            "compositeImage": {
                "id": "a80ee9b3-bcf8-4a84-ac17-08486d50835f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d460cb8c-d9ea-497c-833e-b2c479138e5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb5a880c-cc30-47b7-bdb1-327791dccd88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d460cb8c-d9ea-497c-833e-b2c479138e5c",
                    "LayerId": "be3cab78-e38d-4f36-8803-c2bb70791d0b"
                },
                {
                    "id": "58565622-c2d8-4b7d-a11e-61b2494af2ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d460cb8c-d9ea-497c-833e-b2c479138e5c",
                    "LayerId": "1057c9c3-a521-4a46-972b-348b41ec1866"
                }
            ]
        },
        {
            "id": "2bdf21a7-a25a-4eef-a03c-afd14e955d3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d1b4923-736e-4261-bbbc-24e62e42db2a",
            "compositeImage": {
                "id": "0b1f9f57-9d7a-4b7c-af26-e180e275b1c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bdf21a7-a25a-4eef-a03c-afd14e955d3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "847368bd-b9eb-4689-8b30-8ff3ba61832c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bdf21a7-a25a-4eef-a03c-afd14e955d3a",
                    "LayerId": "be3cab78-e38d-4f36-8803-c2bb70791d0b"
                },
                {
                    "id": "887a3a47-527d-4f1e-a942-68991f13bec6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bdf21a7-a25a-4eef-a03c-afd14e955d3a",
                    "LayerId": "1057c9c3-a521-4a46-972b-348b41ec1866"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "be3cab78-e38d-4f36-8803-c2bb70791d0b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d1b4923-736e-4261-bbbc-24e62e42db2a",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "1057c9c3-a521-4a46-972b-348b41ec1866",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d1b4923-736e-4261-bbbc-24e62e42db2a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 47,
    "yorig": 230
}