{
    "id": "55e3e61b-7114-4df0-902f-37c7b0c3a1c5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_arrow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 2,
    "bbox_right": 30,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c7e500e7-2d92-49f3-80b2-169f3950cbe6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55e3e61b-7114-4df0-902f-37c7b0c3a1c5",
            "compositeImage": {
                "id": "cf1baae5-f928-4d65-b791-193dd7189154",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7e500e7-2d92-49f3-80b2-169f3950cbe6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24ba9048-0187-47a9-a128-4c7f3d732c6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7e500e7-2d92-49f3-80b2-169f3950cbe6",
                    "LayerId": "9ac76caa-6df1-4509-a520-4cc1c5e05830"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9ac76caa-6df1-4509-a520-4cc1c5e05830",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "55e3e61b-7114-4df0-902f-37c7b0c3a1c5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 30,
    "yorig": 16
}