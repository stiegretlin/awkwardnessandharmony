{
    "id": "3fcf5c46-9397-4355-a950-e37899b9d593",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tile01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bf175393-a03c-40d5-a1b3-256b5955986e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3fcf5c46-9397-4355-a950-e37899b9d593",
            "compositeImage": {
                "id": "018f5065-dc00-45cb-a130-1941c043ba0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf175393-a03c-40d5-a1b3-256b5955986e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc57ac31-7e73-4107-830a-e70e69fadcd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf175393-a03c-40d5-a1b3-256b5955986e",
                    "LayerId": "a465db97-d4c7-49b9-882e-7588592976ee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a465db97-d4c7-49b9-882e-7588592976ee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3fcf5c46-9397-4355-a950-e37899b9d593",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}