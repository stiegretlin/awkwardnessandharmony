{
    "id": "db0644f6-bc2f-403d-b791-0b10b8c71fe3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fred_body_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 1,
    "bbox_right": 63,
    "bbox_top": 54,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e2775ebf-a4e8-4778-b665-ce7e5dd75cd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db0644f6-bc2f-403d-b791-0b10b8c71fe3",
            "compositeImage": {
                "id": "3b231b05-cbf5-4cb2-a2ec-49dd087bc958",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2775ebf-a4e8-4778-b665-ce7e5dd75cd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a122fc3b-50f9-4bea-afed-0ea5950a51bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2775ebf-a4e8-4778-b665-ce7e5dd75cd9",
                    "LayerId": "5b792a02-fa09-4f6f-9eae-8bdd6e9cb5bc"
                },
                {
                    "id": "d99c97e3-f953-4603-a97b-a5d3ab3c2eae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2775ebf-a4e8-4778-b665-ce7e5dd75cd9",
                    "LayerId": "92421300-99d2-4f0b-8cfa-dfbae1f24c6d"
                }
            ]
        },
        {
            "id": "52db8f41-674c-4463-9500-fb7a00a1d486",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db0644f6-bc2f-403d-b791-0b10b8c71fe3",
            "compositeImage": {
                "id": "bf3bbaa2-e987-4bda-b636-1f5e82c62390",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52db8f41-674c-4463-9500-fb7a00a1d486",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53bea2d4-f435-42e7-aa4f-f252f4363b43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52db8f41-674c-4463-9500-fb7a00a1d486",
                    "LayerId": "92421300-99d2-4f0b-8cfa-dfbae1f24c6d"
                },
                {
                    "id": "bc4bc722-4ec5-42fa-a9c5-268afe96be67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52db8f41-674c-4463-9500-fb7a00a1d486",
                    "LayerId": "5b792a02-fa09-4f6f-9eae-8bdd6e9cb5bc"
                }
            ]
        },
        {
            "id": "0eaa2dc4-1f0e-4ccc-9f0b-0a381cc4ba18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db0644f6-bc2f-403d-b791-0b10b8c71fe3",
            "compositeImage": {
                "id": "3ea4a09b-ac29-4609-8c97-63ec525f313e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0eaa2dc4-1f0e-4ccc-9f0b-0a381cc4ba18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c606ac4b-74e9-4abf-a3ff-ba1f77fc6b6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0eaa2dc4-1f0e-4ccc-9f0b-0a381cc4ba18",
                    "LayerId": "5b792a02-fa09-4f6f-9eae-8bdd6e9cb5bc"
                },
                {
                    "id": "bb60b9c0-1827-4c64-acf6-e8e7222c3e94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0eaa2dc4-1f0e-4ccc-9f0b-0a381cc4ba18",
                    "LayerId": "92421300-99d2-4f0b-8cfa-dfbae1f24c6d"
                }
            ]
        },
        {
            "id": "fd444652-ca9d-4ae0-82b7-4266faa8f408",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db0644f6-bc2f-403d-b791-0b10b8c71fe3",
            "compositeImage": {
                "id": "5bb399b4-86db-40d5-a619-df9c9bf5fcf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd444652-ca9d-4ae0-82b7-4266faa8f408",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2a675a8-eccb-47d4-85e8-f4e85f862a0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd444652-ca9d-4ae0-82b7-4266faa8f408",
                    "LayerId": "92421300-99d2-4f0b-8cfa-dfbae1f24c6d"
                },
                {
                    "id": "6ba9f7ad-9f83-4db3-9385-030a98214fa4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd444652-ca9d-4ae0-82b7-4266faa8f408",
                    "LayerId": "5b792a02-fa09-4f6f-9eae-8bdd6e9cb5bc"
                }
            ]
        },
        {
            "id": "26ee5dfc-ea0b-44c7-9afd-c96de0dee322",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db0644f6-bc2f-403d-b791-0b10b8c71fe3",
            "compositeImage": {
                "id": "10a3f1d3-d14d-4edb-a8e8-a07d279312f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26ee5dfc-ea0b-44c7-9afd-c96de0dee322",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a545ccbe-3698-456c-8922-cbb2bad27bbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26ee5dfc-ea0b-44c7-9afd-c96de0dee322",
                    "LayerId": "5b792a02-fa09-4f6f-9eae-8bdd6e9cb5bc"
                },
                {
                    "id": "2493fdee-f7aa-4f6d-891e-a2fd52ecceb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26ee5dfc-ea0b-44c7-9afd-c96de0dee322",
                    "LayerId": "92421300-99d2-4f0b-8cfa-dfbae1f24c6d"
                }
            ]
        },
        {
            "id": "711a3e9d-5674-4b1b-9c81-56aad05ea486",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db0644f6-bc2f-403d-b791-0b10b8c71fe3",
            "compositeImage": {
                "id": "0106ceef-260c-4828-8eca-c88917166568",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "711a3e9d-5674-4b1b-9c81-56aad05ea486",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e63c11a-31ad-4f33-941a-1e834790e5ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "711a3e9d-5674-4b1b-9c81-56aad05ea486",
                    "LayerId": "92421300-99d2-4f0b-8cfa-dfbae1f24c6d"
                },
                {
                    "id": "881bdc42-a499-4ab4-991a-726f3dbdd033",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "711a3e9d-5674-4b1b-9c81-56aad05ea486",
                    "LayerId": "5b792a02-fa09-4f6f-9eae-8bdd6e9cb5bc"
                }
            ]
        },
        {
            "id": "aa8c31ee-c3e0-4db4-bef9-f8d255fa349d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db0644f6-bc2f-403d-b791-0b10b8c71fe3",
            "compositeImage": {
                "id": "a1418a4c-6293-4078-bb7b-37941aa3d317",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa8c31ee-c3e0-4db4-bef9-f8d255fa349d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0a03a6b-064a-4426-8249-2359be11030c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa8c31ee-c3e0-4db4-bef9-f8d255fa349d",
                    "LayerId": "92421300-99d2-4f0b-8cfa-dfbae1f24c6d"
                },
                {
                    "id": "20c99378-16b5-474f-9056-43638f7da71f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa8c31ee-c3e0-4db4-bef9-f8d255fa349d",
                    "LayerId": "5b792a02-fa09-4f6f-9eae-8bdd6e9cb5bc"
                }
            ]
        },
        {
            "id": "28c1ccd4-25ad-428c-ba34-2111585e9713",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db0644f6-bc2f-403d-b791-0b10b8c71fe3",
            "compositeImage": {
                "id": "54448051-0afa-48bd-bbfe-f9bd7a99f858",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28c1ccd4-25ad-428c-ba34-2111585e9713",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6aeedfb-57a5-4bd3-b069-c96a3891ebad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28c1ccd4-25ad-428c-ba34-2111585e9713",
                    "LayerId": "92421300-99d2-4f0b-8cfa-dfbae1f24c6d"
                },
                {
                    "id": "f70fc328-77b3-4a12-bd03-5d28213bc68f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28c1ccd4-25ad-428c-ba34-2111585e9713",
                    "LayerId": "5b792a02-fa09-4f6f-9eae-8bdd6e9cb5bc"
                }
            ]
        },
        {
            "id": "fd9e9a02-03c8-43e0-af48-14b973468bf1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db0644f6-bc2f-403d-b791-0b10b8c71fe3",
            "compositeImage": {
                "id": "bf5d7d2d-10f0-41ea-9bdf-493aecd2df79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd9e9a02-03c8-43e0-af48-14b973468bf1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3043fae8-8c92-43f3-9078-091e976e5b3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd9e9a02-03c8-43e0-af48-14b973468bf1",
                    "LayerId": "5b792a02-fa09-4f6f-9eae-8bdd6e9cb5bc"
                },
                {
                    "id": "d149a676-8d8a-45c3-b88f-f000b5eee0b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd9e9a02-03c8-43e0-af48-14b973468bf1",
                    "LayerId": "92421300-99d2-4f0b-8cfa-dfbae1f24c6d"
                }
            ]
        },
        {
            "id": "b3d723b3-9162-49e7-90c5-4e65e364ad02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db0644f6-bc2f-403d-b791-0b10b8c71fe3",
            "compositeImage": {
                "id": "9b3e0712-d4e3-4888-ae49-2eb26b441c5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3d723b3-9162-49e7-90c5-4e65e364ad02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9897d8c1-48be-412e-983b-219df89d34a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3d723b3-9162-49e7-90c5-4e65e364ad02",
                    "LayerId": "92421300-99d2-4f0b-8cfa-dfbae1f24c6d"
                },
                {
                    "id": "3a61e6d3-718a-4b77-8919-f0b1484be924",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3d723b3-9162-49e7-90c5-4e65e364ad02",
                    "LayerId": "5b792a02-fa09-4f6f-9eae-8bdd6e9cb5bc"
                }
            ]
        },
        {
            "id": "ed6fb07f-1f81-4b17-800b-89eab4500807",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db0644f6-bc2f-403d-b791-0b10b8c71fe3",
            "compositeImage": {
                "id": "9d9d1bac-dd4c-4d2f-af4b-c4cbe421fe31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed6fb07f-1f81-4b17-800b-89eab4500807",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e07ea91b-dd26-4507-9bc9-7a941af29dee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed6fb07f-1f81-4b17-800b-89eab4500807",
                    "LayerId": "5b792a02-fa09-4f6f-9eae-8bdd6e9cb5bc"
                },
                {
                    "id": "3e1a9989-b7e5-41cf-94b3-7b17c8aa7f5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed6fb07f-1f81-4b17-800b-89eab4500807",
                    "LayerId": "92421300-99d2-4f0b-8cfa-dfbae1f24c6d"
                }
            ]
        },
        {
            "id": "f0409a36-5468-423e-aa38-d7b2595ddb97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db0644f6-bc2f-403d-b791-0b10b8c71fe3",
            "compositeImage": {
                "id": "5fa60b91-8367-4d5e-92d3-839df0f1616f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0409a36-5468-423e-aa38-d7b2595ddb97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4603c693-c3d3-4af8-ae76-dae9c75b763c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0409a36-5468-423e-aa38-d7b2595ddb97",
                    "LayerId": "92421300-99d2-4f0b-8cfa-dfbae1f24c6d"
                },
                {
                    "id": "76eb288b-1df3-4d9a-bdfd-4f5e54ad698e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0409a36-5468-423e-aa38-d7b2595ddb97",
                    "LayerId": "5b792a02-fa09-4f6f-9eae-8bdd6e9cb5bc"
                }
            ]
        },
        {
            "id": "14f239f9-27b7-4a1d-a59e-acdc54bc3e2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db0644f6-bc2f-403d-b791-0b10b8c71fe3",
            "compositeImage": {
                "id": "64867cfc-8743-47a7-848b-764755e85d76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14f239f9-27b7-4a1d-a59e-acdc54bc3e2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e858f552-7f6a-43b7-8d0e-b0cb76034fee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14f239f9-27b7-4a1d-a59e-acdc54bc3e2e",
                    "LayerId": "5b792a02-fa09-4f6f-9eae-8bdd6e9cb5bc"
                },
                {
                    "id": "2b47f182-c72b-4904-81f6-636f3bf4ab1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14f239f9-27b7-4a1d-a59e-acdc54bc3e2e",
                    "LayerId": "92421300-99d2-4f0b-8cfa-dfbae1f24c6d"
                }
            ]
        },
        {
            "id": "cb2c7879-e993-4cf6-a45f-9f5de49ae06f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db0644f6-bc2f-403d-b791-0b10b8c71fe3",
            "compositeImage": {
                "id": "1db914e6-8e14-456b-9597-a4e03a94289d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb2c7879-e993-4cf6-a45f-9f5de49ae06f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82dd6cb9-1314-4408-994d-446493310013",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb2c7879-e993-4cf6-a45f-9f5de49ae06f",
                    "LayerId": "92421300-99d2-4f0b-8cfa-dfbae1f24c6d"
                },
                {
                    "id": "adf06080-dd5b-40fe-9ec5-f5f127b3883e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb2c7879-e993-4cf6-a45f-9f5de49ae06f",
                    "LayerId": "5b792a02-fa09-4f6f-9eae-8bdd6e9cb5bc"
                }
            ]
        },
        {
            "id": "2dd3dd65-8274-4474-a0e1-c7d729583f24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db0644f6-bc2f-403d-b791-0b10b8c71fe3",
            "compositeImage": {
                "id": "f7a3eed8-3574-49ec-b311-416d6a0fbc2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dd3dd65-8274-4474-a0e1-c7d729583f24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f437b47-4278-4a4d-859e-6f28a9e1026d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dd3dd65-8274-4474-a0e1-c7d729583f24",
                    "LayerId": "5b792a02-fa09-4f6f-9eae-8bdd6e9cb5bc"
                },
                {
                    "id": "8d8d5f0d-5997-4797-8b4d-c350f1b24568",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dd3dd65-8274-4474-a0e1-c7d729583f24",
                    "LayerId": "92421300-99d2-4f0b-8cfa-dfbae1f24c6d"
                }
            ]
        },
        {
            "id": "f56d1a59-f101-48e7-8629-2917f57cb083",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db0644f6-bc2f-403d-b791-0b10b8c71fe3",
            "compositeImage": {
                "id": "9bcb0466-9be1-40dd-ae30-d0cb3efdd55c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f56d1a59-f101-48e7-8629-2917f57cb083",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6303dafe-1913-43a5-b03f-369a690be3b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f56d1a59-f101-48e7-8629-2917f57cb083",
                    "LayerId": "92421300-99d2-4f0b-8cfa-dfbae1f24c6d"
                },
                {
                    "id": "a3342a8b-104a-488e-9aab-ac0a43c5464f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f56d1a59-f101-48e7-8629-2917f57cb083",
                    "LayerId": "5b792a02-fa09-4f6f-9eae-8bdd6e9cb5bc"
                }
            ]
        },
        {
            "id": "72ccb6b2-4a14-45b3-8654-02ffb0d53c15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db0644f6-bc2f-403d-b791-0b10b8c71fe3",
            "compositeImage": {
                "id": "3bda9deb-c335-47a7-a577-3c3b0630d83f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72ccb6b2-4a14-45b3-8654-02ffb0d53c15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad229a9e-49ef-4fcd-ada0-6e7787ad0057",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72ccb6b2-4a14-45b3-8654-02ffb0d53c15",
                    "LayerId": "5b792a02-fa09-4f6f-9eae-8bdd6e9cb5bc"
                },
                {
                    "id": "c3293154-ef23-4e96-83bc-ff812f5c04d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72ccb6b2-4a14-45b3-8654-02ffb0d53c15",
                    "LayerId": "92421300-99d2-4f0b-8cfa-dfbae1f24c6d"
                }
            ]
        },
        {
            "id": "6f3e8ad5-f51c-475b-9b4e-23b6fc5f2925",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db0644f6-bc2f-403d-b791-0b10b8c71fe3",
            "compositeImage": {
                "id": "5c7ad9e5-d36a-4b02-8c33-e124df3a9870",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f3e8ad5-f51c-475b-9b4e-23b6fc5f2925",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78a31f0b-0e86-402c-897f-fdffea72bb5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f3e8ad5-f51c-475b-9b4e-23b6fc5f2925",
                    "LayerId": "92421300-99d2-4f0b-8cfa-dfbae1f24c6d"
                },
                {
                    "id": "afd1a40f-9754-41ce-9d08-0d7344726bd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f3e8ad5-f51c-475b-9b4e-23b6fc5f2925",
                    "LayerId": "5b792a02-fa09-4f6f-9eae-8bdd6e9cb5bc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "92421300-99d2-4f0b-8cfa-dfbae1f24c6d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db0644f6-bc2f-403d-b791-0b10b8c71fe3",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "5b792a02-fa09-4f6f-9eae-8bdd6e9cb5bc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db0644f6-bc2f-403d-b791-0b10b8c71fe3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 8,
            "visible": false
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 30,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 127
}