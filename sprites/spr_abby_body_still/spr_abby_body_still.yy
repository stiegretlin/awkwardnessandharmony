{
    "id": "5e71e3ee-96f0-4895-a9c8-c2906ca7f607",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_abby_body_still",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 4,
    "bbox_right": 60,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "70ef72cf-7cdb-4b20-9693-38742fc78f3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e71e3ee-96f0-4895-a9c8-c2906ca7f607",
            "compositeImage": {
                "id": "3a99f6a2-025d-40fa-9cb6-1bafc900f9b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70ef72cf-7cdb-4b20-9693-38742fc78f3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14743ca8-d397-4e91-99f1-4429c5cb7d4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70ef72cf-7cdb-4b20-9693-38742fc78f3e",
                    "LayerId": "f9d53ba6-4ec1-4de8-be5f-2ea4f927e979"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "f9d53ba6-4ec1-4de8-be5f-2ea4f927e979",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5e71e3ee-96f0-4895-a9c8-c2906ca7f607",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 127
}