{
    "id": "0aae60dc-4fd1-4b34-966e-7f43e557b33e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bark",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 233,
    "bbox_left": -57,
    "bbox_right": 143,
    "bbox_top": -31,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "750b36cb-759e-4c04-b40d-e8328a988fdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0aae60dc-4fd1-4b34-966e-7f43e557b33e",
            "compositeImage": {
                "id": "3ebef21f-8c20-4107-bc75-aca99afed20d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "750b36cb-759e-4c04-b40d-e8328a988fdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fad2c1c5-7af8-4d4e-8ac0-a61f842f04cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "750b36cb-759e-4c04-b40d-e8328a988fdb",
                    "LayerId": "fe2127ed-a5f9-4aa2-aeaf-fc6378cbf44f"
                },
                {
                    "id": "fdb652f4-aaa6-46fe-a131-bce22c770c14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "750b36cb-759e-4c04-b40d-e8328a988fdb",
                    "LayerId": "2bb483f6-b21d-46af-b197-b1b31dfa480e"
                },
                {
                    "id": "7f445894-3c50-4b3c-ba7f-4c4504c654be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "750b36cb-759e-4c04-b40d-e8328a988fdb",
                    "LayerId": "046da5ed-482d-4f72-b7c9-c2a986d9f777"
                }
            ]
        },
        {
            "id": "4b2b2dbc-76fc-480e-af09-10565f492d32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0aae60dc-4fd1-4b34-966e-7f43e557b33e",
            "compositeImage": {
                "id": "21ee1b1a-0853-4003-a97d-f089e150f318",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b2b2dbc-76fc-480e-af09-10565f492d32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6aa3d319-d8d1-47e2-8cf7-ae0f90e0c081",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b2b2dbc-76fc-480e-af09-10565f492d32",
                    "LayerId": "fe2127ed-a5f9-4aa2-aeaf-fc6378cbf44f"
                },
                {
                    "id": "afcf99e9-b408-401b-b77f-29ace85f6cad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b2b2dbc-76fc-480e-af09-10565f492d32",
                    "LayerId": "2bb483f6-b21d-46af-b197-b1b31dfa480e"
                },
                {
                    "id": "cdf21c24-5a9f-455c-8b8f-ddf214045b58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b2b2dbc-76fc-480e-af09-10565f492d32",
                    "LayerId": "046da5ed-482d-4f72-b7c9-c2a986d9f777"
                }
            ]
        },
        {
            "id": "9830880c-4768-4c58-b235-c25d2ab3ae57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0aae60dc-4fd1-4b34-966e-7f43e557b33e",
            "compositeImage": {
                "id": "a79da22d-ed4a-434e-8ff8-6980759aff9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9830880c-4768-4c58-b235-c25d2ab3ae57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0bbf300-0850-4d9e-9efa-481d1cd4ab74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9830880c-4768-4c58-b235-c25d2ab3ae57",
                    "LayerId": "2bb483f6-b21d-46af-b197-b1b31dfa480e"
                },
                {
                    "id": "5356f343-7e82-48b3-8f30-a8aaa978e0ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9830880c-4768-4c58-b235-c25d2ab3ae57",
                    "LayerId": "fe2127ed-a5f9-4aa2-aeaf-fc6378cbf44f"
                },
                {
                    "id": "6053e5c3-1cbe-4a5a-a968-2ff701f20322",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9830880c-4768-4c58-b235-c25d2ab3ae57",
                    "LayerId": "046da5ed-482d-4f72-b7c9-c2a986d9f777"
                }
            ]
        },
        {
            "id": "35b62f61-65f7-451d-abfd-226fa8d873df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0aae60dc-4fd1-4b34-966e-7f43e557b33e",
            "compositeImage": {
                "id": "9b327c33-a799-4e32-a4e5-a48bff322a12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35b62f61-65f7-451d-abfd-226fa8d873df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10e12fbd-7022-4077-84fa-349d7fd38040",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35b62f61-65f7-451d-abfd-226fa8d873df",
                    "LayerId": "fe2127ed-a5f9-4aa2-aeaf-fc6378cbf44f"
                },
                {
                    "id": "0ebcde81-33b6-44fd-a3df-70cd390df74b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35b62f61-65f7-451d-abfd-226fa8d873df",
                    "LayerId": "2bb483f6-b21d-46af-b197-b1b31dfa480e"
                },
                {
                    "id": "06acb6b5-fd50-4679-a368-04d3ddf7ad1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35b62f61-65f7-451d-abfd-226fa8d873df",
                    "LayerId": "046da5ed-482d-4f72-b7c9-c2a986d9f777"
                }
            ]
        },
        {
            "id": "816fb6e6-1df7-4a0a-8643-86f481bb5d49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0aae60dc-4fd1-4b34-966e-7f43e557b33e",
            "compositeImage": {
                "id": "17cd1ec1-e446-41ab-9e97-cf5d57c65535",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "816fb6e6-1df7-4a0a-8643-86f481bb5d49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2217f19-ba4f-4aac-abc9-ee43cef8f250",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "816fb6e6-1df7-4a0a-8643-86f481bb5d49",
                    "LayerId": "fe2127ed-a5f9-4aa2-aeaf-fc6378cbf44f"
                },
                {
                    "id": "d093912c-ea09-474f-84cc-d929fc9d0d68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "816fb6e6-1df7-4a0a-8643-86f481bb5d49",
                    "LayerId": "2bb483f6-b21d-46af-b197-b1b31dfa480e"
                },
                {
                    "id": "2cabef4e-a0e2-45d9-9ed0-3ba98a23b670",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "816fb6e6-1df7-4a0a-8643-86f481bb5d49",
                    "LayerId": "046da5ed-482d-4f72-b7c9-c2a986d9f777"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "046da5ed-482d-4f72-b7c9-c2a986d9f777",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0aae60dc-4fd1-4b34-966e-7f43e557b33e",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1 (2)",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "fe2127ed-a5f9-4aa2-aeaf-fc6378cbf44f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0aae60dc-4fd1-4b34-966e-7f43e557b33e",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "2bb483f6-b21d-46af-b197-b1b31dfa480e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0aae60dc-4fd1-4b34-966e-7f43e557b33e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": false
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 100,
    "xorig": 50,
    "yorig": 152
}