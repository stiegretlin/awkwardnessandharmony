{
    "id": "237047d9-2d92-47c8-9298-25ce12dac162",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "title_screen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 175,
    "bbox_left": 10,
    "bbox_right": 372,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "671ca7ec-9e70-496a-8577-0f0b4dc512b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "237047d9-2d92-47c8-9298-25ce12dac162",
            "compositeImage": {
                "id": "5f9f16b9-e6ee-4add-a57d-548ec27e450f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "671ca7ec-9e70-496a-8577-0f0b4dc512b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc380f5e-7380-4852-8d2b-6065b21f0df4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "671ca7ec-9e70-496a-8577-0f0b4dc512b5",
                    "LayerId": "6e98775e-fcfc-47fc-9559-4ff6753fadfb"
                }
            ]
        },
        {
            "id": "dc6f8653-97c1-48ee-85ea-e8e43e512164",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "237047d9-2d92-47c8-9298-25ce12dac162",
            "compositeImage": {
                "id": "18a9cfeb-4040-4deb-a928-f364f5098ed7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc6f8653-97c1-48ee-85ea-e8e43e512164",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "119ceace-668b-4a37-86fe-75a4298e53f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc6f8653-97c1-48ee-85ea-e8e43e512164",
                    "LayerId": "6e98775e-fcfc-47fc-9559-4ff6753fadfb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "6e98775e-fcfc-47fc-9559-4ff6753fadfb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "237047d9-2d92-47c8-9298-25ce12dac162",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 400,
    "xorig": 0,
    "yorig": 0
}